# LBRY-GTK

## Features:

  * Lists, with thumbnails
    * Search
    * Advanced Search
    * Following
    * Your Tags
    * Discover
    * Library
    * Collections
    * Followed
    * Uploads
    * Channels
  * Wallet
    * Statistics
    * History
  * Open publication pages
    * Channel
    * Video
    * Audio
    * Article
    * Image
    * Collection
  * Open publications in browser
    * [Odysee](https://odysee.com/)
    * [Madiator](https://madiator.com/)
    * [Spee.ch](https://spee.ch/)
    * [Librarian](https://librarian.bcow.xyz/)
  * Comment as any of your channels
    * Open (with markdown support)
    * Write
    * Edit
    * Delete
    * Reply to
    * React to
  * Channel
    * Follow
    * Unfollow
    * Get RSS links
      * [Odysee](https://odysee.com/)
      * [Librarian](https://librarian.bcow.xyz/)
  * Make publications
    * Video
    * Audio
    * Article
    * Image
  * Search tags from publication page
  * Go back and forth
  * Display style
    * Grid
    * List
  * Extensive settings system
  * Specify commands per stream type
    * Video
    * Audio
    * Image
    * Document
  * Advanced Search
    * Text
    * Channels
    * Claims
    * Date
      * Simple
      * Custom
    * Claim Type
    * Stream Type
    * Tags
  * Order by
    * Release Time
    * Name
    * Trending
  * Order Direction
    * Descending
    * Ascending
  * Markdown display (set Document Command to LBRY-GTK-Document)


## Contribution:

Check out the [TODO.md](TODO.md) file

## Links:

Questions and chat about the project: [Matrix](https://matrix.to/#/#LBRY-GTK:matrix.org)

AUR package: [lbry-gtk-git](https://aur.archlinux.org/packages/lbry-gtk-git)

Based on: [FastLBRY-terminal](https://notabug.org/jyamihud/FastLBRY-terminal)

Other great LBRY projects: [Awesome-LBRY](https://github.com/LBRYFoundation/Awesome-LBRY)

## Images:

![](./Images/0.png)
![](./Images/1.png)
![](./Images/2.png)
![](./Images/3.png)
![](./Images/4.png)
![](./Images/5.png)
![](./Images/6.png)
![](./Images/7.png)
![](./Images/8.png)
![](./Images/9.png)
![](./Images/10.png)
![](./Images/11.png)
