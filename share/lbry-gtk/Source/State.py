################################################################################
# LBRY-GTK                                                                     #
# Copyright (C) 2021-2022 MorsMortium and Other Contributors                   #
#                                                                              #
# This program is free software: you can redistribute it and/or modify         #
# it under the terms of the GNU General Public License as published by         #
# the Free Software Foundation, either version 3 of the License, or            #
# (at your option) any later version.                                          #
#                                                                              #
# This program is distributed in the hope that it will be useful,              #
# but WITHOUT ANY WARRANTY; without even the implied warranty of               #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                #
# GNU General Public License for more details.                                 #
#                                                                              #
# You should have received a copy of the GNU General Public License            #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.        #
################################################################################

import gi, json, threading

gi.require_version("Gtk", "3.0")
from gi.repository import GLib


def DifferentLists(List1, List2):
    for Index in range(len(List1)):
        if List1[Index] != List2[Index]:
            return True
    return False


class State:
    def __init__(self):
        self.States, self.CurrentState = [], -1

    def Import(self, FunctionName, Data):
        if isinstance(Data, str):
            Data = json.loads(Data)
        if self.PossibleStates[FunctionName]:
            threading.Thread(
                target=self.PossibleStates[FunctionName],
                args=(Data),
            ).start()

    def Export(self, Function, Data, String=False):
        if String:
            Data = json.dumps(Data)
        FunctionName = ""
        if isinstance(Function, str):
            FunctionName = Function
        else:
            for State in self.PossibleStates:
                if self.PossibleStates[State] == Function:
                    FunctionName = State
                    break
        return [FunctionName, Data]

    def Save(self, Function, Data, Title):
        NoCheck = False
        if self.CurrentState != -1 and self.CurrentState < len(self.States):
            OldState = self.States[self.CurrentState]
        else:
            NoCheck = True
        if (
            NoCheck
            or Function != OldState["Function"]
            or DifferentLists(Data, OldState["Data"])
        ):
            self.CurrentState += 1
            if self.CurrentState < len(self.States):
                self.States[self.CurrentState] = {
                    "Function": Function,
                    "Data": Data,
                    "Title": Title,
                    "Index": self.CurrentState,
                }
            else:
                self.States.append(
                    {
                        "Function": Function,
                        "Data": Data,
                        "Title": Title,
                        "Index": self.CurrentState,
                    }
                )

    def Back(self):
        if 0 < self.CurrentState:
            self.CurrentState -= 1
            CurrentState = self.States[self.CurrentState]
            threading.Thread(
                target=CurrentState["Function"],
                args=([*CurrentState["Data"], True]),
            ).start()

    def Forward(self):
        if self.CurrentState < len(self.States) - 1:
            self.CurrentState += 1
            CurrentState = self.States[self.CurrentState]
            threading.Thread(
                target=CurrentState["Function"],
                args=([*CurrentState["Data"], True]),
            ).start()

    def Previous(self):
        if 0 < self.CurrentState:
            Previous = self.CurrentState - 1
            return self.Export(
                self.States[Previous]["Function"], self.States[Previous]["Data"]
            )
        return ""

    def Next(self):
        if self.CurrentState < len(self.States) - 1:
            Next = self.CurrentState + 1
            return self.Export(
                self.States[Next]["Function"], self.States[Next]["Data"]
            )
        return ""

    def Before(self):
        List = []
        for Index in range(self.CurrentState):
            List.append(self.States[Index])
        return List

    def After(self):
        List = []
        for Index in range(self.CurrentState + 1, len(self.States)):
            List.append(self.States[Index])
        return List

    def Goto(self, Index):
        if -1 < Index and Index < len(self.States):
            self.CurrentState = Index
            CurrentState = self.States[self.CurrentState]
            threading.Thread(
                target=CurrentState["Function"],
                args=([*CurrentState["Data"], True]),
            ).start()
