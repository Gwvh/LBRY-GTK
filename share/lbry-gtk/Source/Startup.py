################################################################################
# LBRY-GTK                                                                     #
# Copyright (C) 2021-2022 MorsMortium and Other Contributors                   #
#                                                                              #
# This program is free software: you can redistribute it and/or modify         #
# it under the terms of the GNU General Public License as published by         #
# the Free Software Foundation, either version 3 of the License, or            #
# (at your option) any later version.                                          #
#                                                                              #
# This program is distributed in the hope that it will be useful,              #
# but WITHOUT ANY WARRANTY; without even the implied warranty of               #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                #
# GNU General Public License for more details.                                 #
#                                                                              #
# You should have received a copy of the GNU General Public License            #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.        #
################################################################################

import gi, math, os, json

gi.require_version("Gtk", "3.0")
from gi.repository import Gtk, Gdk, GLib, GdkPixbuf

from flbry import connect, wallet, settings

from Source import Places, Error

DefaultSettings = {
    "LinkSite": 0,
    "LinkCommand": "",
    "RSSSite": 0,
    "PlayCommand": "",
    "ImageCommand": "",
    "VideoCommand": "",
    "AudioCommand": "",
    "DocumentCommand": "LBRY-GTK-Document",
    "ThumbnailHeight": 72.0,
    "ThumbnailWidth": 128.0,
    "ReplyIndent": 5.0,
    "ListDisplay": 0,
    "ContentPerLoading": 10.0,
    "CommentPerLoading": 10.0,
    "CommentServer": "https://comments.odysee.com/api/v2",
    "EnableComments": True,
    "Profile": True,
    "ProfileHeight": 64.0,
    "ProfileWidth": 64.0,
    "GridTitleRows": 2.0,
    "GridAuthorRows": 1.0,
    "GridPadding": 1.0,
    "CommentChannel": "",
    "ImageType": 1,
    "ImageHeight": 256.0,
    "ImageWidth": 256.0,
    "ImageScale": 1.0,
    "CommentImageType": 1,
    "CommentImageHeight": 128.0,
    "CommentImageWidth": 128.0,
    "CommentImageScale": 1.0,
    "EnableMarkdown": True,
    "MouseBackAndForward": True,
    "OrderBy": 0,
    "Direction": 0,
    "HomeType": 0,
    "HomeFunction": "Help",
    "HomeData": "[]",
    "PromoteLBRYGTK": False,
    "NotTags": [],
    "AuthToken": "",
    "EnableMetaService": True,
    "MetaServer": "api.odysee.com",
}

DefaultSession = {
    "Start": True,
    "Stop": True,
    "Binary": "lbrynet start",
    "Server": "http://localhost:5279",
    "Timeout": 30.0,
    "NewBinary": "",
    "NewServer": "",
    "Minimized": False,
    "Tray": True,
    "TrayMinimized": False,
    "WindowSize": False,
    "WindowHeight": 480.0,
    "WindowWidth": 640.0,
    "PageWidth": 128.0,
    "PageExpand": True,
}


class Startup:
    Started = False

    def __init__(self, *args):
        (
            self.Builder,
            self.Window,
            self.Balance,
            self.ExitQueue,
            self.Stater,
            self.Title,
        ) = args

        self.Startup = self.Builder.get_object("Startup")
        self.StartupProgress = self.Builder.get_object("StartupProgress")
        self.StatusMenu = self.Builder.get_object("StatusMenu")
        self.ShowHide = self.Builder.get_object("ShowHide")

        Builder = self.Builder

        self.Status = {
            "blob_manager": Builder.get_object("BlobManager"),
            "database": Builder.get_object("Database"),
            "dht": Builder.get_object("DHT"),
            "exchange_rate_manager": Builder.get_object("ExchangeRateManager"),
            "file_manager": Builder.get_object("FileManager"),
            "hash_announcer": Builder.get_object("HashAnnouncer"),
            "libtorrent_component": Builder.get_object("LibtorrentComponent"),
            "peer_protocol_server": Builder.get_object("PeerProtocolServer"),
            "upnp": Builder.get_object("UPNP"),
            "wallet": Builder.get_object("Wallet"),
            "wallet_server_payments": Builder.get_object("WalletServerPayment"),
            "background_downloader": Builder.get_object("BackgroundDownloader"),
            "disk_space": Builder.get_object("DiskSpace"),
            "LBRY-GTK": Builder.get_object("LBRY-GTK"),
        }

        try:
            os.makedirs(Places.CacheDir)
        except:
            pass

        try:
            os.makedirs(Places.ConfigDir)
        except:
            pass

        try:
            with open(Places.ConfigDir + "Session.json", "r") as File:
                json.load(File)
        except:
            with open(Places.ConfigDir + "Session.json", "w") as File:
                json.dump(DefaultSession, File)

    def StartupHelper(self):
        self.Started = False
        self.Replace("Startup")
        self.Title.set_text("Status")

        with open(Places.ConfigDir + "Session.json", "r") as File:
            Session = json.load(File)

        for Setting in DefaultSession:
            if not (
                Setting in Session
                and isinstance(Session[Setting], type(DefaultSession[Setting]))
            ):
                Session[Setting] = DefaultSession[Setting]

        if Session["NewBinary"] != "":
            Session["Binary"] = Session["NewBinary"]

        if Session["NewServer"] != "":
            Session["Server"] = Session["NewServer"]

        with open(Places.ConfigDir + "Session.json", "w") as File:
            json.dump(Session, File)

        GLib.idle_add(self.WindowSettings, Session)

        if Session["Start"]:
            Connected = connect.start(
                self.UpdateStartupProgress,
                self.ExitQueue,
                Session["Binary"],
                Session["Timeout"],
                server=Session["Server"],
            )
            ErrorMessage = "LBRYNet could not be started."
        else:
            Connected = connect.check_timeout(
                self.UpdateStartupProgress,
                Session["Timeout"],
                server=Session["Server"],
            )
            ErrorMessage = "LBRYNet is not running."

        if not Connected:
            Error.Error(ErrorMessage, self.Window)
            return

        LBRYSettings = settings.get(server=Session["Server"])
        if isinstance(LBRYSettings, str):
            Error.Error(LBRYSettings, self.Window)
            return
        if not "shared" in LBRYSettings["preferences"]:
            LBRYSettings["preferences"]["shared"] = {}
        if not "local" in LBRYSettings["preferences"]:
            LBRYSettings["preferences"]["local"] = {}

        Shared = LBRYSettings["preferences"]["shared"]
        Local = LBRYSettings["preferences"]["local"]

        if not "value" in Shared:
            Shared["value"] = {}

        if not "value" in Local:
            Local["value"] = {}

        Shared = Shared["value"]
        Local = Local["value"]

        if not "subscriptions" in Shared:
            Shared["subscriptions"] = []
        if not "following" in Shared:
            Shared["following"] = []
        if not "settings" in Shared:
            Shared["settings"] = {}
        if not "LBRY-GTK" in Shared:
            Shared["LBRY-GTK"] = {}
        if not "tags" in Shared:
            Shared["tags"] = []
        if not "tags" in Local:
            Local["tags"] = []

        if not "show_mature" in Shared["settings"]:
            Shared["settings"]["show_mature"] = False

        Settings = Shared["LBRY-GTK"]

        for Setting in DefaultSettings:
            if not (
                Setting in Settings
                and isinstance(
                    Settings[Setting], type(DefaultSettings[Setting])
                )
            ):
                Settings[Setting] = DefaultSettings[Setting]

        settings.set(LBRYSettings, server=Session["Server"])

        GLib.idle_add(self.FinishStartup)

        self.Started = True
        self.Stater.Import(Settings["HomeFunction"], Settings["HomeData"])

        GLib.idle_add(self.UpdateBalance, False)
        GLib.timeout_add(60 * 1000, self.UpdateBalance, True)

    def ShowStatusThread(self, State=False):
        if not State:
            self.Stater.Save(self.ShowStatusThread, [], "Status")
        with open(Places.ConfigDir + "Session.json", "r") as File:
            Session = json.load(File)
        Status = connect.status(server=Session["Server"])
        Status["LBRY-GTK"] = self.Started
        GLib.idle_add(self.ShowStatusUpdate, Status)

    def ShowStatusUpdate(self, Status):
        self.Replace("Startup")
        self.Title.set_text("Status")
        self.UpdateStartupProgressHelper(Status)

    def FinishStartup(self):
        self.Status["LBRY-GTK"].set_active(True)
        self.StartupProgress.set_fraction(1)
        self.StartupProgress.show_all()

    def WindowSettings(self, Session):
        if Session["Minimized"]:
            self.Window.iconify()
        else:
            self.Window.show_all()

        if Session["Tray"]:
            with open(Places.ImageDir + "lbry-gtk.svg", "r") as File:
                Svg = File.read()
            Loader = GdkPixbuf.PixbufLoader()
            Loader.write(Svg.encode())
            Loader.close()
            self.StatusIcon = Gtk.StatusIcon.new_from_pixbuf(
                Loader.get_pixbuf()
            )
            self.StatusIcon.set_title("LBRY-GTK")
            self.StatusIcon.connect("activate", self.on_ShowHide_activate)
            self.StatusIcon.connect("popup-menu", self.on_StatusIcon_popup_menu)
            self.StatusIcon.set_visible(True)
            if Session["TrayMinimized"]:
                self.ShowHide.set_label("Show")
                self.Window.hide()

        if Session["WindowSize"]:
            self.Window.resize(Session["WindowWidth"], Session["WindowHeight"])

    def on_Quit_activate(self, Widget):
        self.Window.destroy()

    def UpdateStartupProgressHelper(self, Status):
        Started = 0
        for CheckBox in Status:
            self.Status[CheckBox].set_active(False)
            if Status[CheckBox]:
                Started += 1
                self.Status[CheckBox].set_active(True)
                self.Status[CheckBox].show_all()

        self.StartupProgress.set_fraction(Started / len(self.Status))
        self.StartupProgress.show_all()

    def UpdateStartupProgress(self, Status):
        GLib.idle_add(self.UpdateStartupProgressHelper, Status)

    def NoToggleBefore(self, Widget):
        Widget.set_name(str(Widget.get_active()))

    def NoToggleAfter(self, Widget):
        Widget.set_active(Widget.get_name() == "True")

    def on_ShowHide_activate(self, Widget):
        if self.Window.get_visible():
            self.ShowHide.set_label("Show")
            self.Window.hide()
        else:
            self.ShowHide.set_label("Hide")
            self.Window.show_all()

    def on_StatusIcon_popup_menu(self, Widget, Button, Time):
        self.StatusMenu.popup(None, None, None, None, Button, Time)

    def UpdateBalance(self, ReturnValue):
        with open(Places.ConfigDir + "Session.json", "r") as File:
            Session = json.load(File)
        GotBalance = wallet.balance(server=Session["Server"])
        if isinstance(GotBalance, str):
            Error.Error(GotBalance, self.Window)
        else:
            Balance = float(GotBalance["total"])
            self.Balance.set_label(str(math.trunc(Balance)))
            self.Balance.set_tooltip_text(str(Balance))
            self.Balance.show_all()
        return ReturnValue
