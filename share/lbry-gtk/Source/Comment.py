################################################################################
# LBRY-GTK                                                                     #
# Copyright (C) 2021-2022 MorsMortium and Other Contributors                   #
#                                                                              #
# This program is free software: you can redistribute it and/or modify         #
# it under the terms of the GNU General Public License as published by         #
# the Free Software Foundation, either version 3 of the License, or            #
# (at your option) any later version.                                          #
#                                                                              #
# This program is distributed in the hope that it will be useful,              #
# but WITHOUT ANY WARRANTY; without even the implied warranty of               #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                #
# GNU General Public License for more details.                                 #
#                                                                              #
# You should have received a copy of the GNU General Public License            #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.        #
################################################################################

import gi

gi.require_version("Gtk", "3.0")
from gi.repository import Gtk, GdkPixbuf, Gdk

from flbry import error

from Source import Image, Places
from Source.Error import Error
from Source.Open import Open
from Source.Channels import Channels
from Source.Markdown import Markdown
from Source.CommentControlTop import CommentControlTop
from Source.CommentControlMiddle import CommentControlMiddle
from Source.CommentControlBottom import CommentControlBottom

Promo = "\n\nSent from [*LBRY-GTK*](https://codeberg.org/MorsMortium/LBRY-GTK)"


class Comment:
    def __init__(self, *args):
        (
            self.Builder,
            self.ShowHiderTexts,
            self.ClaimID,
            self.Box,
            self.Row,
            self.Boxes,
            self.Window,
            self.SingleComment,
            self.GetPublication,
            HiddenUI,
            Start,
            self.ReplyIndent,
            self.CommentServer,
            self.Settings,
            self.ChannelList,
            self.Stater,
            self.AddPage,
            PChannel,
        ) = args

        self.Comment = self.Builder.get_object("Comment")
        self.CommentTextBox = self.Builder.get_object("CommentTextBox")
        self.RepliesFrame = self.Builder.get_object("RepliesFrame")
        self.RepliesExpander = self.Builder.get_object("RepliesExpander")
        self.RepliesBox = self.Builder.get_object("RepliesBox")
        self.Profile = self.Builder.get_object("Profile")
        self.ProfileBox = self.Builder.get_object("ProfileBox")
        self.PreviewBox = self.Builder.get_object("PreviewBox")
        self.CCTopBox = self.Builder.get_object("CommentControlTopBox")
        self.CCMiddleBox = self.Builder.get_object("CommentControlMiddleBox")
        self.CCBottomBox = self.Builder.get_object("CommentControlBottomBox")
        self.ReplyScrolled = self.Builder.get_object("ReplyScrolled")
        self.ReplyText = self.Builder.get_object("ReplyText")

        Builder = Gtk.Builder.new_from_file(Places.GladeDir + "Channels.glade")
        self.Channelser = Channels(
            Builder,
            False,
            self.Window,
            self.ChannelList,
            self.Settings,
        )
        Builder.connect_signals(self.Channelser)
        self.Channels = self.Channelser.Channels

        self.Markdowns = [
            {"Text": self.Row[3], "Box": self.CommentTextBox},
            {"Text": "", "Box": self.PreviewBox},
        ]

        for MarkdownItem in self.Markdowns:
            MarkdownItem["MarkdownBuilder"] = Gtk.Builder.new_from_file(
                Places.GladeDir + "Markdown.glade"
            )
            MarkdownItem["Markdowner"] = Markdown(
                MarkdownItem["MarkdownBuilder"],
                self.Window,
                self.GetPublication,
                MarkdownItem["Text"],
                "",
                True,
                self.Settings["EnableMarkdown"],
            )
            MarkdownItem["MarkdownBuilder"].connect_signals(
                MarkdownItem["Markdowner"]
            )
            MarkdownItem["Box"].pack_start(
                MarkdownItem["Markdowner"].Document, True, True, 0
            )
            self.ShowHiderTexts.append(MarkdownItem["Markdowner"].TextBox)
            MarkdownItem["Markdowner"].Fill()

        Builder = Gtk.Builder.new_from_file(
            Places.GladeDir + "CommentControlBottom.glade"
        )
        self.CCBottomer = CommentControlBottom(
            Builder,
            self.Window,
            self.Row,
            self.Channelser,
            self.Markdowns,
            self.Box,
            self.ClaimID,
            self.CommentServer,
            self.ChannelList,
            self.SingleComment,
            self.Boxes,
            self.ReplyIndent,
            self.Settings,
            self.RepliesFrame,
            self.RepliesBox,
            self.RepliesExpander,
            HiddenUI,
            PChannel,
            self.ReplyText,
            Promo,
            self.on_ReplyText_key_release_event,
        )
        Builder.connect_signals(self.CCBottomer)
        self.CCBottomBox.pack_start(
            self.CCBottomer.CommentControlBottom, True, True, 0
        )
        self.ChannelsBox2 = self.CCBottomer.ChannelsBox2
        self.Post = self.CCBottomer.Post
        self.Save = self.CCBottomer.Save
        self.Tip = self.CCBottomer.Tip
        self.TipLabel1 = self.CCBottomer.TipLabel1
        self.TipLabel2 = self.CCBottomer.TipLabel2

        Builder = Gtk.Builder.new_from_file(
            Places.GladeDir + "CommentControlTop.glade"
        )
        self.CCToper = CommentControlTop(
            Builder,
            self.Window,
            self.Row,
            self.GetPublication,
            self.ChannelList,
            self.CommentServer,
            self.Box,
            self.ShowHiderTexts,
            self.Markdowns,
            self.Comment,
            self.ReplyText,
            self.ReplyScrolled,
            self.PreviewBox,
            self.Save,
            self.on_ReplyText_key_release_event,
            self.Stater,
            self.AddPage,
        )
        Builder.connect_signals(self.CCToper)
        self.CCTopBox.pack_start(self.CCToper.CommentControlTop, True, True, 0)
        self.Channel = self.CCToper.Channel
        self.Support = self.CCToper.Support
        self.Delete = self.CCToper.Delete
        self.Edit = self.CCToper.Edit

        Builder = Gtk.Builder.new_from_file(
            Places.GladeDir + "CommentControlMiddle.glade"
        )
        self.CCMiddler = CommentControlMiddle(
            Builder,
            self.Window,
            self.Row,
            self.Channelser,
            self.Edit,
            self.CommentServer,
            self.ReplyScrolled,
            self.Post,
            self.PreviewBox,
            self.Tip,
            self.TipLabel1,
            self.TipLabel2,
            PChannel,
            self.ChannelList,
        )
        Builder.connect_signals(self.CCMiddler)
        self.CCMiddleBox.pack_start(
            self.CCMiddler.CommentControlMiddle, True, True, 0
        )
        self.Heart = self.CCMiddler.Heart
        self.Like = self.CCMiddler.Like
        self.Dislike = self.CCMiddler.Dislike
        self.LikeNumber = self.CCMiddler.LikeNumber
        self.DislikeNumber = self.CCMiddler.DislikeNumber
        self.Reply = self.CCMiddler.Reply
        self.ChannelsBox1 = self.CCMiddler.ChannelsBox1

        self.CCBottomer.on_Edit_clicked = self.CCToper.on_Edit_clicked
        self.CCBottomer.Edit = self.Edit
        self.CCBottomer.on_Reply_clicked = self.CCMiddler.on_Reply_clicked
        self.CCBottomer.Reply = self.Reply

        Display = self.ProfileBox.get_display()
        self.DefaultCursor = Gdk.Cursor.new_from_name(Display, "default")
        self.PointerCursor = Gdk.Cursor.new_from_name(Display, "pointer")
        self.TextCursor = Gdk.Cursor.new_from_name(Display, "text")

        if self.Settings["Profile"]:
            self.ProfileBox.set_no_show_all(False)
            self.ProfileBox.show()
            self.ProfileBox.set_name(self.Row[6])
            Pixbuf = GdkPixbuf.Pixbuf.new(
                GdkPixbuf.Colorspace.RGB,
                True,
                8,
                self.Settings["ProfileWidth"],
                self.Settings["ProfileHeight"],
            )
            self.Profile.set_from_pixbuf(Pixbuf)
            Image.FillPixbuf(
                self.Row[6], Pixbuf, self.Profile.set_from_pixbuf, Pixbuf
            )
        else:
            self.ProfileBox.set_no_show_all(True)
            self.ProfileBox.hide()

        self.RepliesFrame.set_margin_start(self.ReplyIndent)

        for Channel in self.ChannelList:
            if Channel[3] == PChannel:
                self.Heart.set_no_show_all(False)
                self.Heart.set_sensitive(True)
                break

        if self.Row[11]:
            self.Heart.set_no_show_all(False)

        if HiddenUI:
            self.ChannelsBox2.add(self.Channels)
            self.ReplyScrolled.set_visible(True)
            self.Post.set_visible(True)
            self.PreviewBox.set_visible(True)
            self.Tip.set_visible(True)
            self.TipLabel1.set_visible(True)
            self.TipLabel2.set_visible(True)
            ToHide = [
                self.Channel,
                self.Support,
                self.Heart,
                self.Like,
                self.Dislike,
                self.Delete,
                self.Edit,
                self.CommentTextBox,
                self.Reply,
                self.Profile,
                self.Save,
                self.LikeNumber,
                self.DislikeNumber,
            ]
            for Widget in ToHide:
                Widget.set_no_show_all(True)
                Widget.hide()
        else:
            self.ChannelsBox1.add(self.Channels)

        if Start:
            self.Box.reorder_child(self.Comment, 0)

        self.Box.add(self.Comment)

        self.Channel.set_label(self.Row[2])
        self.Support.set_label(str(self.Row[1]) + " LBC")

        self.ShowHiderTexts.append(self.ReplyText)
        if self.Row[2] != "":
            for Channel in self.ChannelList:
                if Channel[0] == self.Row[2]:
                    self.Edit.set_no_show_all(False)
                    self.Delete.set_no_show_all(False)
                    break

        if self.Row[0] != 0:
            self.RepliesFrame.set_no_show_all(False)
            self.Boxes[self.Row[4]] = self.RepliesBox
            self.RepliesExpander.set_label("Replies (" + str(self.Row[0]) + ")")

        self.Box.show_all()

    def on_ProfileBox_enter_notify_event(self, Widget, Discard=""):
        Widget.get_window().set_cursor(self.PointerCursor)

    def on_ProfileBox_leave_notify_event(self, Widget, Discard=""):
        Widget.get_window().set_cursor(self.DefaultCursor)

    def on_ProfileBox_button_press_event(self, Widget, Event):
        if Event.button != 8 and Event.button != 9:
            Url = Widget.get_name()
            if Url != "":
                ImageCommand = self.Settings["ImageCommand"]
                try:
                    Open(Image.Path(Url), ImageCommand)
                except Exception as e:
                    Error(error.error(e), self.Window)
            else:
                Error(
                    "This publication does not have a thumbnail.", self.Window
                )

    def on_ReplyText_draw(self, Widget, Discard=""):
        self.ReplyScrolled.get_vadjustment().set_value(0)

    def on_ReplyText_key_release_event(self, Widget, Discard=""):
        TextBuffer = Widget.get_buffer()
        Start = TextBuffer.get_start_iter()
        End = TextBuffer.get_end_iter()
        Text = TextBuffer.get_text(Start, End, False)
        if (
            self.Settings["PromoteLBRYGTK"]
            and Text != ""
            and not self.Save.get_visible()
        ):
            Text += Promo
        self.Markdowns[1]["Text"] = Text
        self.Markdowns[1]["Markdowner"].Text = self.Markdowns[1]["Text"]
        self.Markdowns[1]["Markdowner"].Fill()
        self.Box.show_all()
