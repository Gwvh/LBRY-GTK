################################################################################
# LBRY-GTK                                                                     #
# Copyright (C) 2021-2022 MorsMortium and Other Contributors                   #
#                                                                              #
# This program is free software: you can redistribute it and/or modify         #
# it under the terms of the GNU General Public License as published by         #
# the Free Software Foundation, either version 3 of the License, or            #
# (at your option) any later version.                                          #
#                                                                              #
# This program is distributed in the hope that it will be useful,              #
# but WITHOUT ANY WARRANTY; without even the implied warranty of               #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                #
# GNU General Public License for more details.                                 #
#                                                                              #
# You should have received a copy of the GNU General Public License            #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.        #
################################################################################

import gi, threading, time, os, re, queue, json

gi.require_version("Gtk", "3.0")
from gi.repository import Gtk, GLib, Gdk

from flbry import url, channel

from Source import Places, Settings
from Source.Open import Open
from Source.FetchComment import FetchComment
from Source.Error import Error
from Source.Thumbnail import Thumbnail
from Source.Document import Document
from Source.Tag import Tag
from Source.PublicationControl import PublicationControl


def Replace(Match):
    String = Match.string[Match.start() : Match.end()]
    return "<a href='%s'>%s</a>" % (String, String)


class Publication:
    Queue = queue.Queue()

    def __init__(self, *args):
        (
            self.Builder,
            self.ShowHiderTexts,
            self.Window,
            self.Stater,
            self.Title,
            self.MainSpace,
            self.AddPage,
        ) = args

        Builder = self.Builder

        self.Publication = Builder.get_object("Publication")
        self.DataView = Builder.get_object("DataView")
        self.TagsExpander = Builder.get_object("TagsExpander")
        self.DescriptionExpander = Builder.get_object("DescriptionExpander")
        self.LinksExpander = Builder.get_object("LinksExpander")
        self.CommentExpander = Builder.get_object("CommentExpander")
        self.ThumbnailDataBox = Builder.get_object("ThumbnailDataBox")
        self.Description = Builder.get_object("Description")
        self.DataStore = Builder.get_object("DataStore")
        self.TagBox = Builder.get_object("TagBox")
        self.Links = Builder.get_object("Links")
        self.PublicationControlBox = Builder.get_object("PublicationControlBox")

        self.DataView.set_search_equal_func(self.SearchFunction)

        self.Documenter = Document(
            self.Window,
            self.GetPublication,
            self.Stater,
            self.ShowHiderTexts,
            self.Title,
            self.MainSpace,
        )

        Builder = Gtk.Builder.new_from_file(Places.GladeDir + "Thumbnail.glade")
        self.Thumbnailer = Thumbnail(Builder, self.Window)
        Builder.connect_signals(self.Thumbnailer)
        self.ThumbnailDataBox.pack_start(
            self.Thumbnailer.Thumbnail, True, True, 0
        )

        Builder = Gtk.Builder.new_from_file(
            Places.GladeDir + "PublicationControl.glade"
        )
        self.PublicationControler = PublicationControl(
            Builder,
            self.Window,
            self.GetPublication,
            self.Documenter,
            self.Links,
            self.Stater,
            self.AddPage,
        )
        Builder.connect_signals(self.PublicationControler)
        self.PublicationControlBox.pack_start(
            self.PublicationControler.PublicationControl, True, True, 0
        )

        Builder = Gtk.Builder.new_from_file(Places.GladeDir + "Tag.glade")
        self.Tagger = Tag(Builder, False)
        Builder.connect_signals(self.Tagger)
        self.TagBox.pack_start(self.Tagger.Tag, True, True, 0)

        self.Commenter = FetchComment(
            self.Window,
            self.ShowHiderTexts,
            self.GetPublication,
            self.Stater,
            self.AddPage,
        )

    def ShowLinks(self, Links):
        self.Links.forall(self.Links.remove)
        for Link in Links:
            LinkButton = Gtk.LinkButton.new_with_label(Link[1], Link[0])
            LinkButton.set_halign(Gtk.Align.START)
            self.Links.add(LinkButton)
        self.Links.show_all()

    def GetLinks(self, Url):
        Links = url.web(Url)
        GLib.idle_add(self.ShowLinks, Links)

    def GetTitle(self, Data):
        Title = ""
        try:
            Title = Data["signing_channel"]["value"]["title"] + " - "
        except:
            pass
        try:
            Title = Title + Data["value"]["title"]
        except:
            Title = Title + Data["name"][1:]
        return Title

    def GetPublication(self, Url, Check=False, State=False):
        self.Queue.put(True)

        with open(Places.ConfigDir + "Session.json", "r") as File:
            Session = json.load(File)
        Data = url.get([Url], server=Session["Server"])[0]
        if isinstance(Data, str):
            if not Check:
                Error(Data, self.Window)
            else:
                Check.put(True)
            return
        if Check:
            Check.put(False)
        NewTitle = self.GetTitle(Data)
        if not State:
            self.Stater.Save(self.GetPublication, [Url, Check], NewTitle)
        GLib.idle_add(self.ShowPublication, Data)

    def ShowPublication(self, Data):
        self.Replace("Publication")
        self.DescriptionExpander.set_expanded(False)
        self.CommentExpander.set_expanded(False)
        self.LinksExpander.set_expanded(False)
        self.TagsExpander.set_expanded(False)

        NewTitle = self.GetTitle(Data)

        ClaimID = Data["claim_id"]

        try:
            Channel = Data["signing_channel"]["permanent_url"]
            ChannelID = Data["signing_channel"]["claim_id"]
        except:
            Channel = Data["permanent_url"]
            ChannelID = Data["claim_id"]

        self.PublicationControler.ShowPublicationControl(Data, NewTitle)

        self.Title.set_text(NewTitle)

        # This seems to be overly complicated
        Unneeded = ["value_description", "value_tags"]
        Tocheck = [Data]
        TocheckNames = ["data"]
        Branches = {"data": None}
        BranchPlaces = {str(Data): None}
        i = 0
        self.DataStore.clear()
        while i < len(Tocheck):
            Branch = Tocheck[i]
            BranchName = TocheckNames[i]
            if BranchName != "data":
                Branches[BranchName] = self.DataStore.append(
                    BranchPlaces[str(Branch)], (str(BranchName), "")
                )
            if isinstance(Branch, dict):
                Iter = Branch.items()
            else:
                Iter = enumerate(Branch)
            for Key, Value in Iter:
                if isinstance(Value, dict) or isinstance(Value, list):
                    BranchPlaces[str(Value)] = Branches[BranchName]
                    TocheckNames.append(Key)
                    Tocheck.append(Value)
                else:
                    if not (str(BranchName) + "_" + str(Key)) in Unneeded:
                        self.DataStore.append(
                            Branches[BranchName], (str(Key), str(Value))
                        )
            i += 1

        try:
            Description = GLib.markup_escape_text(
                str(Data["value"]["description"])
            )
        except:
            Description = "No description"

        Url = "http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+"
        self.Description.set_label(re.sub(Url, Replace, Description))

        self.Tagger.RemoveAll()
        try:
            self.Tagger.Append(Data["value"]["tags"])
        except:
            pass

        TagFlowChildren = len(self.Tagger.Tags)
        if TagFlowChildren == 0:
            self.TagsExpander.set_label("No Tags")
            self.Tagger.Add("I told you")
        else:
            self.TagsExpander.set_label("Tags (" + str(TagFlowChildren) + ")")

        try:
            self.Thumbnailer.Url = Data["value"]["thumbnail"]["url"]
        except:
            self.Thumbnailer.Url = ""

        args = [Data["canonical_url"]]
        threading.Thread(target=self.GetLinks, args=(args)).start()

        del self.ShowHiderTexts[:]
        self.Publication.show_all()
        LBRYSettings = Settings.Get()
        if isinstance(LBRYSettings, str):
            Error(LBRYSettings, self.Window)
            return
        Session = LBRYSettings["Session"]
        LBRYSettings = LBRYSettings["preferences"]["shared"]["value"][
            "LBRY-GTK"
        ]
        if LBRYSettings["EnableComments"]:
            ChannelList = channel.channel_list(server=Session["Server"])
            CommentServer = LBRYSettings["CommentServer"]
            self.CommentExpander.show()
            CommentBox = Gtk.Box.new(Gtk.Orientation.VERTICAL, 0)
            self.Commenter.SingleComment(
                ClaimID,
                CommentBox,
                [0, 0, "", "", "", "", "", 0, 0, False, False, False],
                {"Root": CommentBox},
                CommentServer,
                True,
                False,
                0,
                LBRYSettings,
                ChannelList,
                Channel,
            )
            self.CommentExpander.remove(self.CommentExpander.get_children()[0])
            self.CommentExpander.add(CommentBox)
            self.Queue.put(True)
            self.Queue = queue.Queue()
            Args = [
                ClaimID,
                CommentBox,
                self.Queue,
                CommentServer,
                Channel,
            ]
            threading.Thread(
                target=self.Commenter.Fetch, args=(Args), daemon=True
            ).start()
            self.CommentExpander.set_label("Comments")
            Args = [self.CommentExpander, ChannelID, CommentServer]
            threading.Thread(
                target=self.Commenter.DisplayPrice, args=(Args)
            ).start()
        else:
            self.CommentExpander.hide()

    def SearchFunction(self, Model, Column, Key, Iter):
        Row = Model[Iter]
        if Key.lower() in list(Row)[Column].lower():
            return False

        for Inner in Row.iterchildren():
            if Key.lower() in list(Inner)[Column].lower():
                self.DataView.expand_to_path(Row.path)
                break
        else:
            self.DataView.collapse_row(Row.path)
        return True

    def on_Description_activate_link(self, Widget, Link):
        LBRYSettings = Settings.Get()
        if isinstance(LBRYSettings, str):
            Error(LBRYSettings, self.Window)
            return True
        LBRYSettings = LBRYSettings["preferences"]["shared"]["value"][
            "LBRY-GTK"
        ]
        Open(Link, LBRYSettings["LinkCommand"])
        return True
