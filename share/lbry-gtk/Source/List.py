################################################################################
# LBRY-GTK                                                                     #
# Copyright (C) 2021-2022 MorsMortium and Other Contributors                   #
#                                                                              #
# This program is free software: you can redistribute it and/or modify         #
# it under the terms of the GNU General Public License as published by         #
# the Free Software Foundation, either version 3 of the License, or            #
# (at your option) any later version.                                          #
#                                                                              #
# This program is distributed in the hope that it will be useful,              #
# but WITHOUT ANY WARRANTY; without even the implied warranty of               #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                #
# GNU General Public License for more details.                                 #
#                                                                              #
# You should have received a copy of the GNU General Public License            #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.        #
################################################################################

import gi, threading, json, queue, copy

gi.require_version("Gtk", "3.0")
from gi.repository import Gtk, GLib, GdkPixbuf, Pango

from flbry import search, wallet, list_files

from Source import Image, Places, Settings, ListGridUtil
from Source.Error import Error
from Source.Box import Box

Orderings = ["creation_height", "name", "trending_mixed"]
ClaimTypes = {"stream": 1, "channel": 2, "repost": 3, "collection": 4}


def ColumnMaker(TreeView, Columns, Cells):
    for i in range(len(Columns)):
        if isinstance(Cells[i], Gtk.CellRendererPixbuf):
            NewColumn = Gtk.TreeViewColumn(Columns[i], Cells[i], pixbuf=i)
        else:
            if isinstance(Cells[i], Gtk.CellRendererText):
                NewColumn = Gtk.TreeViewColumn(Columns[i], Cells[i], text=i)
                if (
                    Cells[i].get_property("ellipsize")
                    == Pango.EllipsizeMode.END
                ):
                    NewColumn.set_expand(True)
            else:
                NewColumn = Gtk.TreeViewColumn(Columns[i], Cells[i], active=i)

        TreeView.append_column(NewColumn)


Cells = {
    "Default": Gtk.CellRendererText(),
    "Ellipsized": Gtk.CellRendererText(),
    "Edit": Gtk.CellRendererText(),
    "Pixbuf": Gtk.CellRendererPixbuf(),
    "Toggle": Gtk.CellRendererToggle(),
}

Cells["Ellipsized"].set_property("ellipsize", Pango.EllipsizeMode.END)
Cells["Edit"].set_property("editable", True)
Cells["Toggle"].set_activatable(True)

RowCells = [
    Cells["Pixbuf"],
    Cells["Default"],
    Cells["Default"],
    Cells["Toggle"],
    Cells["Default"],
    Cells["Ellipsized"],
    Cells["Default"],
]

DataCells = [Cells["Default"], Cells["Edit"]]

EmptyPixbuf = GdkPixbuf.Pixbuf()
EmptyPixbuf = EmptyPixbuf.add_alpha(False, 0, 0, 0)
EmptyPixbuf.fill(0x00000000)


class List:
    ListPage, Loaded, Started = 0, 1, False

    def __init__(self, *args):
        (
            self.View,
            self.DataView,
            self.Window,
            self.Publicationer,
            self.Stores,
            self.WalletSpaceParts,
            self.MainSpace,
            self.Stater,
            self.Grid,
            self.ContentSpace,
            self.WalletSpace,
            self.FileSpace,
            self.Text,
            self.OrderBy,
            self.Direction,
            self.Channel,
            self.ClaimType,
            self.AnyTagger,
            self.AllTagger,
            self.NotTagger,
            self.ClaimIdTagger,
            self.ChannelIdTagger,
            self.DateTimer,
            self.Inequality,
            self.Advanced,
            self.StreamTyper,
            self.DateType,
            self.Title,
            self.AddPage,
        ) = args

        Columns = [
            "Thumbnail",
            "Confirmations",
            "Amount",
            "Is Tip",
            "Channel",
            "Title",
            "Type",
        ]
        ColumnMaker(self.View, Columns, RowCells)

        Columns = ["Name", "Value"]
        ColumnMaker(self.DataView, Columns, DataCells)

        Builder = Gtk.Builder.new_from_file(Places.GladeDir + "Box.glade")
        Height = Builder.get_object("Type").get_preferred_height()
        self.TextHeight = Height.minimum_height
        Height = Builder.get_object("IsTip").get_preferred_height()
        self.CheckHeight = Height.minimum_height

        self.ListFunctions = {
            "Search": [search.simple],
            "Wallet": [self.BalanceHelper, wallet.history],
            "File": [list_files.downloaded],
        }
        self.Threads = []

    def UpdateMainSpace(self, Increase=0):
        self.Loaded += Increase
        for Thread in self.Threads:
            Thread[1].get()
        self.ListThread(*self.ListFunctions[self.ListButton])

    def Empty(self, Queue):
        self.Stores["Row"].clear()
        self.Grid.forall(Gtk.Widget.destroy)
        Queue.put(True)

    def ListThread(self, ListFunction, *args):
        ThreadQueue = queue.Queue()
        UpdateMainSpaceQueue = queue.Queue()
        self.Threads.append([ThreadQueue, UpdateMainSpaceQueue])
        LBRYSettings = Settings.Get()
        if isinstance(LBRYSettings, str):
            Error(LBRYSettings, self.Window)
            self.Threads.remove([ThreadQueue, UpdateMainSpaceQueue])
            UpdateMainSpaceQueue.put(True)
            return
        Server = LBRYSettings["Session"]["Server"]
        LBRYSettings = LBRYSettings["preferences"]["shared"]["value"][
            "LBRY-GTK"
        ]
        ContentPerLoading = int(LBRYSettings["ContentPerLoading"])
        Height = int(LBRYSettings["ThumbnailHeight"])
        Width = int(LBRYSettings["ThumbnailWidth"])
        if self.ListPage == 0:
            EmptyQueue = queue.Queue()
            GLib.idle_add(self.Empty, EmptyQueue)
            EmptyQueue.get()
        for Index in range(self.ListPage, self.Loaded):
            Data = ListFunction(
                *args,
                **self.Data,
                page_size=ContentPerLoading,
                page=Index + 1,
                server=Server,
            )
            if isinstance(Data, str):
                Error(Data, self.Window)
                self.ListPage = Index - 1
                self.Threads.remove([ThreadQueue, UpdateMainSpaceQueue])
                UpdateMainSpaceQueue.put(True)
                return
            self.ListNumber += len(Data)
            Store = self.Stores["Row"]
            for Row in Data:
                try:
                    ThreadQueue.get(block=False)
                    self.Threads.remove([ThreadQueue, UpdateMainSpaceQueue])
                    UpdateMainSpaceQueue.put(True)
                    return
                except:
                    pass
                EmptyCopy = EmptyPixbuf.scale_simple(
                    Width, Height, GdkPixbuf.InterpType.NEAREST
                )
                Args = [Row, EmptyCopy, self.MainSpace, self.List]
                if self.ListDisplay == 0:
                    GLib.idle_add(ListGridUtil.ListUpdate, *Args, Store)
                else:
                    GLib.idle_add(
                        ListGridUtil.GridUpdate,
                        *Args,
                        LBRYSettings,
                        self.Publicationer,
                        self.Grid,
                        self.AddPage,
                        self.Stater,
                    )
        self.ListPage = self.Loaded
        self.Started = True
        self.Threads.remove([ThreadQueue, UpdateMainSpaceQueue])
        UpdateMainSpaceQueue.put(True)

    def GenericLoaded(self, Fill=False):
        LBRYSettings = Settings.Get()
        if isinstance(LBRYSettings, str):
            Error(LBRYSettings, self.Window)
            return
        LBRYSettings = LBRYSettings["preferences"]["shared"]["value"][
            "LBRY-GTK"
        ]
        Height = int(LBRYSettings["ThumbnailHeight"])
        Width = int(LBRYSettings["ThumbnailWidth"])
        ContentPerLoading = int(LBRYSettings["ContentPerLoading"])
        ContentQueue = queue.Queue()
        if self.ListDisplay == 0:
            ContentFunction = ListGridUtil.ListContent
        else:
            ContentFunction = ListGridUtil.GridContent
        GLib.idle_add(
            ContentFunction,
            Height,
            Width,
            self.MainSpace,
            ContentQueue,
            LBRYSettings,
            self.List,
            self.CheckHeight,
            self.TextHeight,
        )
        Content = ContentQueue.get()
        self.Loaded = Content // ContentPerLoading + 1
        if Fill and self.ListPage < self.Loaded:
            self.UpdateMainSpace()

    def ButtonThread(self, ButtonName, ListName, Title, Data={}, State=False):
        self.Started = False
        LBRYSettings = Settings.Get()
        if isinstance(LBRYSettings, str):
            Error(self.Window, LBRYSettings)
            return
        LBRYGTKSettings = LBRYSettings["preferences"]["shared"]["value"][
            "LBRY-GTK"
        ]
        self.ListDisplay = LBRYGTKSettings["ListDisplay"]
        self.Data = Data
        if not State:
            self.Stater.Save(
                self.ButtonThread, [ButtonName, ListName, Title, Data], Title
            )
        GLib.idle_add(
            self.ButtonHelper, ButtonName, ListName, Title, Data, LBRYSettings
        )

    def ButtonHelper(self, ButtonName, ListName, Title, Data, LBRYSettings):
        LBRYGTKSettings = LBRYSettings["preferences"]["shared"]["value"][
            "LBRY-GTK"
        ]
        self.Advanced.set_expanded(False)
        self.OrderBy.set_active(LBRYGTKSettings["OrderBy"])
        self.Direction.set_active(LBRYGTKSettings["Direction"])
        self.Text.set_text("")
        self.Channel.set_text("")
        self.ClaimType.set_active(0)
        self.AnyTagger.RemoveAll()
        self.AllTagger.RemoveAll()
        self.NotTagger.RemoveAll()
        self.ClaimIdTagger.RemoveAll()
        self.ChannelIdTagger.RemoveAll()
        self.StreamTyper.RemoveAll()
        self.DateTimer.DateUse.set_active(False)
        self.DateTimer.on_DateReset_clicked()
        self.Inequality.set_active(0)
        self.DateType.set_active(0)

        if "order_by" in Data:
            Order = Data["order_by"][0]
            if Order.startswith("^"):
                self.Direction.set_active(1)
                Order = Order[1:]
            else:
                self.Direction.set_active(0)
            for Index in range(len(Orderings)):
                if Order == Orderings[Index]:
                    self.OrderBy.set_active(Index)
        else:
            if ListName == "Content":
                Data["order_by"] = [Orderings[self.OrderBy.get_active()]]
                if self.Direction.get_active() == 1:
                    Data["order_by"][0] = ["^" + Data["order_by"][0]]
        if "text" in Data:
            self.Text.set_text(Data["text"])
        if "channel" in Data:
            self.Channel.set_text(Data["channel"])
        if "claim_type" in Data:
            self.ClaimType.set_active(ClaimTypes[Data["claim_type"]])
        if "any_tags" in Data:
            self.AnyTagger.Append(Data["any_tags"])
        if "all_tags" in Data:
            self.AllTagger.Append(Data["all_tags"])
        if "not_tags" in Data:
            self.NotTagger.Append(Data["not_tags"])
        if (
            ListName == "Content"
            and not LBRYSettings["preferences"]["shared"]["value"]["settings"][
                "show_mature"
            ]
        ):
            if "not_tags" in Data:
                Data["not_tags"].append("mature")
            else:
                Data["not_tags"] = ["mature"]
        if ListName == "Content" and LBRYGTKSettings["NotTags"] != []:
            if "not_tags" in Data:
                Data["not_tags"].extend(
                    copy.deepcopy(LBRYGTKSettings["NotTags"])
                )
            else:
                Data["not_tags"] = (
                    copy.deepcopy(LBRYGTKSettings["NotTags"])[0],
                )
        if "claim_ids" in Data:
            self.ClaimIdTagger.Append(Data["claim_ids"])
        if "channel_ids" in Data:
            self.ChannelIdTagger.Append(Data["channel_ids"])
        if "stream_types" in Data:
            self.StreamTyper.Append(Data["stream_types"])
        if "timestamp" in Data:
            Time = Data["timestamp"]
            if Time != "-1":
                self.DateType.set_active(2)
                self.DateTimer.DateUse.set_active(True)
                if Time.startswith("<="):
                    self.Inequality.set_active(3)
                    Time = Time[2:]
                elif Time.startswith(">="):
                    self.Inequality.set_active(4)
                    Time = Time[2:]
                elif Time.startswith("<"):
                    self.Inequality.set_active(0)
                    Time = Time[1:]
                elif Time.startswith(">"):
                    self.Inequality.set_active(1)
                    Time = Time[1:]
                else:
                    self.Inequality.set_active(2)
                self.DateTimer.SetTime(int(Time))

        self.ListButton = ButtonName
        self.List = ListName
        self.Title.set_text(Title)
        Parent = self.Grid.get_parent()
        if Parent:
            Parent.remove(self.Grid)
        Parent = self.View.get_parent()
        if Parent:
            Parent.remove(self.View)
        Space = self.ContentSpace
        if self.List == "Wallet":
            Space = self.WalletSpace
        elif self.List == "File":
            Space = self.FileSpace
        Item = self.Grid
        if self.ListDisplay == 0:
            Item = self.View
            for Index in range(1, 4):
                Item.get_column(Index).set_visible(self.List == "Wallet")
        Space.add(Item)
        self.Replace(self.List + "List")
        self.Publicationer.Queue.put(True)
        self.ListNumber = 0
        self.ListPage = 0
        for Thread in self.Threads:
            Thread[0].put(True)
        threading.Thread(target=self.GenericLoaded, args=([True])).start()

    def BalanceHelper(self, Function, **kwargs):
        with open(Places.ConfigDir + "Session.json", "r") as File:
            Session = json.load(File)
        GotBalance = wallet.balance(server=Session["Server"])
        if isinstance(GotBalance, str):
            Error(GotBalance, self.Window)
            return Function(**kwargs)
        Values = [
            GotBalance["total"],
            GotBalance["available"],
            GotBalance["reserved"],
            GotBalance["reserved_subtotals"]["claims"],
            GotBalance["reserved_subtotals"]["tips"],
            GotBalance["reserved_subtotals"]["supports"],
        ]
        for i in range(6):
            self.WalletSpaceParts[i].set_label(str(Values[i]))
        return Function(**kwargs)
