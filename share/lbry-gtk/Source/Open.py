################################################################################
# LBRY-GTK                                                                     #
# Copyright (C) 2021-2022 MorsMortium and Other Contributors                   #
#                                                                              #
# This program is free software: you can redistribute it and/or modify         #
# it under the terms of the GNU General Public License as published by         #
# the Free Software Foundation, either version 3 of the License, or            #
# (at your option) any later version.                                          #
#                                                                              #
# This program is distributed in the hope that it will be useful,              #
# but WITHOUT ANY WARRANTY; without even the implied warranty of               #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                #
# GNU General Public License for more details.                                 #
#                                                                              #
# You should have received a copy of the GNU General Public License            #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.        #
################################################################################

import sys, subprocess


def Open(Uri, Application="", Quiet=True):
    if Application != "":
        Opener = Application
    elif sys.platform == "win32":
        Opener = "start"
        Uri = '" "' + Uri
    elif sys.platform == "darwin":
        Opener = "open"
    else:
        Opener = "xdg-open"

    Command = Opener + ' "' + Uri + '"'

    if Quiet:
        return subprocess.call(
            Command,
            stdout=subprocess.DEVNULL,
            stderr=subprocess.STDOUT,
            shell=True,
        )
    else:
        return subprocess.call(Command, shell=True)
