################################################################################
# LBRY-GTK                                                                     #
# Copyright (C) 2021-2022 MorsMortium and Other Contributors                   #
#                                                                              #
# This program is free software: you can redistribute it and/or modify         #
# it under the terms of the GNU General Public License as published by         #
# the Free Software Foundation, either version 3 of the License, or            #
# (at your option) any later version.                                          #
#                                                                              #
# This program is distributed in the hope that it will be useful,              #
# but WITHOUT ANY WARRANTY; without even the implied warranty of               #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                #
# GNU General Public License for more details.                                 #
#                                                                              #
# You should have received a copy of the GNU General Public License            #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.        #
################################################################################

import re, gi, threading, queue

gi.require_version("Gtk", "3.0")
from gi.repository import Gtk, GdkPixbuf, Gdk

from Source import Image, Places, Settings, MDParse
from Source.Open import Open
from Source.Error import Error

Items = ["<a", "<hr", "&lt;!--", "<img", "<blockquote>"]


def Empty(*Args):
    return ""


def RemoveA(Start, Text):
    LinkText = Text[Start:]
    HrefStart = LinkText.find('"') + 1
    Link = LinkText[HrefStart : LinkText.find('"', HrefStart)]
    LabelStart = LinkText.find(">") + 1
    LabelEnd = LinkText.find("</a>", LabelStart)
    End = LabelEnd + 4
    Label = LinkText[LabelStart:LabelEnd]
    Length = len(re.sub("<[^>]*>", "", Label))
    Insert = ""
    if Find(Label):
        Length = 2
    elif Length == 0:
        Insert = Link
        Length = len(Link)
    return (Text[:Start] + Insert + Label + LinkText[End:], [Link, Length])


def RemoveImg(Start, Text):
    LinkText = Text[Start:]
    HrefStart = LinkText.find('"') + 1
    Link = LinkText[HrefStart : LinkText.find('"', HrefStart)]
    End = LinkText.find("/>") + 2
    return (Text[:Start] + " " + LinkText[End:], Link)


def RemoveHR(Start, Text):
    LinkText = Text[Start:]
    End = LinkText.find("/>") + 2
    return (Text[:Start] + LinkText[End:], "")


def RemoveC(Start, Text):
    return (Text[:Start] + Text[(Text.find("--&gt;") + 6) :], "")


def RemoveQuotes(Start, Text):
    QuoteText = Text[Start:]
    QuoteStart = QuoteText.find(">") + 1
    QuoteTextCopy = QuoteText
    Starts, Ends = 0, 0
    while True:
        PossibleEnd = QuoteTextCopy.find("</blockquote>")
        PossibleStart = QuoteTextCopy.find("<blockquote>")
        if PossibleStart != -1 and PossibleStart < PossibleEnd:
            Starts += 1
            QuoteTextCopy = QuoteTextCopy.replace("<blockquote>", "", 1)
        elif PossibleEnd != -1:
            Ends += 1
            QuoteTextCopy = QuoteTextCopy.replace("</blockquote>", "", 1)
        if Starts == Ends:
            break
    End = PossibleEnd + Starts * 12 + Ends * 13
    Quote = QuoteText[QuoteStart : End - 13]
    CleanQuote = re.sub("<[^>]*>", "", Quote)
    return (Text[:Start] + Quote + QuoteText[End:], len(CleanQuote))


Removers = {
    "<a": RemoveA,
    "<hr": RemoveHR,
    "&lt;!--": RemoveC,
    "<img": RemoveImg,
    "<blockquote>": RemoveQuotes,
}


def LinkClick(
    TextTag, Widget, Event, Iter, GetPublication, LinkCommand, Path, Markdown
):
    if Event.type == Gdk.EventType.BUTTON_PRESS:
        Widget.set_name(TextTag.get_property("name"))
    if Event.type == Gdk.EventType.BUTTON_RELEASE:
        Name = Widget.get_name()
        if Name == TextTag.get_property("name"):

            if Name.startswith(".") and Name.endswith(".md"):
                try:
                    with open(Path + Name[2:], "r") as File:
                        Text = File.read()
                    Window = Widget.get_window(Gtk.TextWindowType.TEXT)
                    Markdown.Text = Text
                    Markdown.Fill()
                except:
                    pass
            else:
                PublicationQueue = queue.Queue()
                GetPublication(Name, PublicationQueue)
                if PublicationQueue.get():
                    Open(Name, LinkCommand)
            Widget.set_name("")


def InsertA(TextView, Index, Data, Settings, IfComment, GetPublication, *args):
    Link, Length = Data
    if Length != 0:
        TextBuffer = TextView.get_buffer()
        StartIter = TextBuffer.get_iter_at_offset(Index)
        TagTable = TextBuffer.get_tag_table()
        LinkTag = TagTable.lookup(Link)
        if LinkTag == None:
            LinkTag = TextBuffer.create_tag(Link)
        EndIter = TextBuffer.get_iter_at_offset(Index + Length)
        TextBuffer.apply_tag(LinkTag, StartIter, EndIter)
        TextBuffer.apply_tag_by_name("Link", StartIter, EndIter)
        LinkTag.connect(
            "event", LinkClick, GetPublication, Settings["LinkCommand"], *args
        )


def InsertImg(
    TextView, Index, Link, Settings, IfComment, GetPublication, Path, *args
):
    GetResolution = Image.GetUrlResolution
    FillPixbuf = Image.FillPixbuf
    if Link.startswith("."):
        Link = Path + Link[2:]
        GetResolution = Image.GetPathResolution
        FillPixbuf = Image.FillPixbufFill
    if IfComment:
        ImageType = Settings["CommentImageType"]
        MaxImageWidth = Settings["CommentImageWidth"]
        MaxImageHeight = Settings["CommentImageHeight"]
        ImageScale = Settings["CommentImageScale"]
    else:
        ImageType = Settings["ImageType"]
        MaxImageWidth = Settings["ImageWidth"]
        MaxImageHeight = Settings["ImageHeight"]
        ImageScale = Settings["ImageScale"]

    TextBuffer = TextView.get_buffer()
    Iter = TextBuffer.get_iter_at_offset(Index)
    TextBuffer.insert(Iter, " ")

    threading.Thread(
        target=GetResolution,
        args=(
            [
                Link,
                InsertImgUpdate,
                TextView,
                Index,
                Link,
                ImageType,
                ImageScale,
                MaxImageWidth,
                MaxImageHeight,
                FillPixbuf,
            ]
        ),
        daemon=True,
    ).start()


def InsertImgUpdate(
    TextView,
    Index,
    Link,
    ImageType,
    ImageScale,
    MaxImageWidth,
    MaxImageHeight,
    FillPixbuf,
    Size,
):
    if ImageType == 0:
        Pixbuf = GdkPixbuf.Pixbuf.new(GdkPixbuf.Colorspace.RGB, True, 8, *Size)
    elif ImageType == 2:
        Resolution = [*Size]
        Resolution[0] *= ImageScale
        Resolution[1] *= ImageScale
        Pixbuf = GdkPixbuf.Pixbuf.new(
            GdkPixbuf.Colorspace.RGB, True, 8, *Resolution
        )
    else:
        ImageWidth, ImageHeight = Size
        WidthRatio = MaxImageWidth / ImageWidth
        HeightRatio = MaxImageHeight / ImageHeight

        Ratio = min(WidthRatio, HeightRatio)

        NewWidth = int(ImageWidth * Ratio)
        NewHeight = int(ImageHeight * Ratio)
        if ImageWidth < NewWidth:
            NewWidth = ImageWidth
            NewHeight = ImageHeight

        Pixbuf = GdkPixbuf.Pixbuf.new(
            GdkPixbuf.Colorspace.RGB, True, 8, NewWidth, NewHeight
        )
    GImage = Gtk.Image.new_from_pixbuf(Pixbuf)
    FillPixbuf(Link, Pixbuf, GImage.set_from_pixbuf, Pixbuf)
    TextBuffer = TextView.get_buffer()
    StartIter = TextBuffer.get_iter_at_offset(Index)
    EndIter = TextBuffer.get_iter_at_offset(Index + 1)
    TextBuffer.delete(StartIter, EndIter)
    Anchor = TextBuffer.create_child_anchor(StartIter)
    TextView.add_child_at_anchor(GImage, Anchor)
    TextView.show_all()


def BoxSize(Widget, Discard):
    Widget.set_size_request(Widget.get_parent().get_allocated_width(), -1)


def InsertHR(TextView, Index, *args):
    TextBuffer = TextView.get_buffer()
    Iter = TextBuffer.get_iter_at_offset(Index)
    Separator = Gtk.Separator.new(Gtk.Orientation.HORIZONTAL)
    Box = Gtk.Box()
    Box.pack_start(Separator, True, True, 0)
    Box.connect("draw", BoxSize)
    Anchor = TextBuffer.create_child_anchor(Iter)
    TextView.add_child_at_anchor(Box, Anchor)


def QuoteDraw(TextView, Discard, Widget, StartIndex, EndIndex):
    TextBuffer = TextView.get_buffer()
    StartIter = TextBuffer.get_iter_at_offset(StartIndex + 1)
    EndIter = TextBuffer.get_iter_at_offset(EndIndex)
    MaxHeight = TextView.get_allocated_height()
    EndLocation = TextView.get_iter_location(EndIter)
    Widget.set_margin_top(TextView.get_iter_location(StartIter).y)
    if 0 < MaxHeight - EndLocation.height - EndLocation.y:
        Widget.set_margin_bottom(MaxHeight - EndLocation.height - EndLocation.y)
    Widget.show()


def InsertQuote(TextView, Index, Length, *args):
    TextBuffer = TextView.get_buffer()
    TagTable = TextBuffer.get_tag_table()
    Separator = Gtk.Separator.new(Gtk.Orientation.VERTICAL)
    Separator.set_halign(Gtk.Align.START)
    Separator.set_no_show_all(True)
    Separator.hide()
    TextView.connect("draw", QuoteDraw, Separator, Index, Index + Length)
    TextView.get_parent().get_parent().add_overlay(Separator)
    StartIter = TextBuffer.get_iter_at_offset(Index)
    EndIter = TextBuffer.get_iter_at_offset(Index + Length)
    for Level in range(10, 0, -1):
        if StartIter.has_tag(TagTable.lookup("Quote" + str(Level))):
            if Level == 10:
                Separator.set_margin_start(270)
                TextBuffer.apply_tag_by_name("Quote10", StartIter, EndIter)
            else:
                Separator.set_margin_start(Level * 30)
                TextBuffer.apply_tag_by_name(
                    "Quote" + str(Level + 1), StartIter, EndIter
                )
            break
    if not StartIter.has_tag(TagTable.lookup("Quote1")):
        TextBuffer.apply_tag_by_name("Quote1", StartIter, EndIter)


Inserters = {
    "<a": InsertA,
    "<hr": InsertHR,
    "&lt;!--": Empty,
    "<img": InsertImg,
    "<blockquote>": InsertQuote,
}


def Find(Text):
    Length = len(Text)
    ClosestItem = ""
    for Item in Items:
        if Text.find(Item) != -1 and Text.find(Item) < Length:
            Length = Text.find(Item)
            ClosestItem = Item
    if Length != len(Text):
        return ClosestItem
    return False


def Count(Replacings, Type):
    Number = 0
    for Replacing in Replacings:
        if Replacing[0] == Type:
            Number += 1
    return Number


def Fill(Text, TextView, Window, *args):
    LBRYSettings = Settings.Get()
    if isinstance(LBRYSettings, str):
        Error(LBRYSettings, Window)
        return
    LBRYSettings = LBRYSettings["preferences"]["shared"]["value"]["LBRY-GTK"]
    Text = MDParse.Parse(Text)
    Replacings = []
    while Find(Text):
        Item = Find(Text)
        Start = Text.find(Item)
        Text, Data = Removers[Item](Start, Text)
        Length = (
            len(re.sub("</", "", re.sub("<[^>]*>", "", Text[:Start])))
            + Count(Replacings, "<img")
            + Count(Replacings, "<hr")
        )

        Replacings.append([Item, Length, Data])
    TextBuffer = TextView.get_buffer()
    TextBuffer.set_text("")
    Text += "\n"
    Text = re.sub("&", "&amp;", Text)
    TextBuffer.insert_markup(TextBuffer.get_start_iter(), Text, -1)
    for Replacing in Replacings:
        Length, Data = Replacing[1], Replacing[2]
        Inserters[Replacing[0]](TextView, Length, Data, LBRYSettings, *args)
