################################################################################
# LBRY-GTK                                                                     #
# Copyright (C) 2021-2022 MorsMortium and Other Contributors                   #
#                                                                              #
# This program is free software: you can redistribute it and/or modify         #
# it under the terms of the GNU General Public License as published by         #
# the Free Software Foundation, either version 3 of the License, or            #
# (at your option) any later version.                                          #
#                                                                              #
# This program is distributed in the hope that it will be useful,              #
# but WITHOUT ANY WARRANTY; without even the implied warranty of               #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                #
# GNU General Public License for more details.                                 #
#                                                                              #
# You should have received a copy of the GNU General Public License            #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.        #
################################################################################

import gi

gi.require_version("Gtk", "3.0")
from gi.repository import GLib

from flbry import channel, list_files

from Source import Places, Settings
from Source.Error import Error


def GetChannels():
    ChannelIds = []
    LBRYSettings = Settings.Get()
    if isinstance(LBRYSettings, str):
        Error(LBRYSettings, self.Window)
        return ["-1"]
    Page = 1
    while True:
        NewChannels = channel.channel_list(
            page=Page, server=LBRYSettings["Session"]["Server"]
        )
        Page += 1
        for Channel in NewChannels:
            ChannelIds.append(Channel[3])
        if len(NewChannels) != 5:
            break
    if ChannelIds == []:
        ChannelIds = ["-1"]
    return ChannelIds


class StateThread:
    def __init__(self, *args):
        (
            self.Stater,
            self.NewPublicationer,
            self.Publicationer,
            self.Settingser,
            self.Lister,
        ) = args

    def NewPublicationThread(self, State=False):
        if not State:
            self.Stater.Save(self.NewPublicationThread, [], "New Publication")
        GLib.idle_add(self.NewPublicationer.ShowNewPublication)

    def SettingsThread(self, State=False):
        if not State:
            self.Stater.Save(self.SettingsThread, [], "Settings")
        GLib.idle_add(self.Settingser.ShowSettings)

    def HelpThread(self, State=False):
        if not State:
            self.Stater.Save(self.HelpThread, [], "Document: Help")
        GLib.idle_add(
            self.Publicationer.Documenter.Display,
            Places.HelpDir + "Index.md",
            "Help",
            True,
        )

    def FollowingThread(self, State=False):
        LBRYSettings = Settings.Get()
        if isinstance(LBRYSettings, str):
            Error(LBRYSettings, self.Window)
            return
        if not State:
            self.Stater.Save(self.FollowingThread, [], "Following")
        ChannelIds = []
        Subscriptions = LBRYSettings["preferences"]["shared"]["value"][
            "subscriptions"
        ]
        for Channel in Subscriptions:
            ChannelIds.append(Channel)
        self.Lister.ButtonThread(
            "Search",
            "Content",
            "Following",
            {"channel_ids": ChannelIds},
            True,
        )

    def DiscoverThread(self, State=False):
        if not State:
            self.Stater.Save(self.DiscoverThread, [], "Discover")
        self.Lister.ButtonThread(
            "Search",
            "Content",
            "Discover",
            {"order_by": ["trending_mixed"]},
            True,
        )

    def UploadsThread(self, State=False):
        LBRYSettings = Settings.Get()
        if isinstance(LBRYSettings, str):
            Error(LBRYSettings, self.Window)
            return
        if not State:
            self.Stater.Save(self.UploadsThread, [], "Uploads")
        self.Lister.ButtonThread(
            "Search",
            "Content",
            "Uploads",
            {"channel_ids": GetChannels()},
            True,
        )

    def LibrarySearchThread(self, State=False):
        LBRYSettings = Settings.Get()
        if isinstance(LBRYSettings, str):
            Error(LBRYSettings, self.Window)
            return
        if not State:
            self.Stater.Save(self.LibrarySearchThread, [], "Library Search")
        DownloadIds = []
        Page = 1
        while True:
            Downloads = list_files.downloaded(
                page=Page,
                page_size=100,
                server=LBRYSettings["Session"]["Server"],
            )
            Page += 1
            for Download in Downloads:
                DownloadIds.append(Download[3])
            if len(Downloads) != 100:
                break
        if DownloadIds == []:
            DownloadIds = ["-1"]
        self.Lister.ButtonThread(
            "Search",
            "Content",
            "Library Search",
            {"claim_ids": DownloadIds},
            True,
        )

    def LibraryThread(self, State=False):
        LBRYSettings = Settings.Get()
        if isinstance(LBRYSettings, str):
            Error(LBRYSettings, self.Window)
            return
        if not State:
            self.Stater.Save(self.LibraryThread, [], "Library")
        self.Lister.ButtonThread("File", "File", "Library", {}, True)

    def ChannelsThread(self, State=False):
        LBRYSettings = Settings.Get()
        if isinstance(LBRYSettings, str):
            Error(LBRYSettings, self.Window)
            return
        if not State:
            self.Stater.Save(self.ChannelsThread, [], "Channels")
        self.Lister.ButtonThread(
            "Search",
            "Content",
            "Channels",
            {"claim_ids": GetChannels()},
            True,
        )

    def FollowedThread(self, State=False):
        LBRYSettings = Settings.Get()
        if isinstance(LBRYSettings, str):
            Error(LBRYSettings, self.Window)
            return
        if not State:
            self.Stater.Save(self.FollowedThread, [], "Followed")
        ChannelIds = []
        Subscriptions = LBRYSettings["preferences"]["shared"]["value"][
            "subscriptions"
        ]
        for Channel in Subscriptions:
            ChannelIds.append(Channel)
        self.Lister.ButtonThread(
            "Search",
            "Content",
            "Followed",
            {"claim_ids": ChannelIds},
            True,
        )

    def CollectionsThread(self, State=False):
        LBRYSettings = Settings.Get()
        if isinstance(LBRYSettings, str):
            Error(LBRYSettings, self.Window)
            return
        if not State:
            self.Stater.Save(self.CollectionsThread, [], "Collections")
        self.Lister.ButtonThread(
            "Search",
            "Content",
            "Collections",
            {"channel_ids": GetChannels(), "claim_type": "collection"},
            True,
        )

    def YourTagsThread(self, State=False):
        LBRYSettings = Settings.Get()
        if isinstance(LBRYSettings, str):
            Error(LBRYSettings, self.Window)
            return
        if not State:
            self.Stater.Save(self.YourTagsThread, [], "Your Tags")
        Tags = LBRYSettings["preferences"]["shared"]["value"]["tags"]
        Tags.extend(LBRYSettings["preferences"]["local"]["value"]["tags"])
        self.Lister.ButtonThread(
            "Search",
            "Content",
            "Your Tags",
            {"any_tags": Tags},
            True,
        )

    def HomeThread(self):
        LBRYSettings = Settings.Get()
        if isinstance(LBRYSettings, str):
            Error(LBRYSettings, self.Window)
            return
        LBRYSettings = LBRYSettings["preferences"]["shared"]["value"][
            "LBRY-GTK"
        ]
        self.Stater.Import(
            LBRYSettings["HomeFunction"],
            LBRYSettings["HomeData"],
        )
