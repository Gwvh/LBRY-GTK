This page is for documenting LBRY-GTKs markdown rendering capabilities.<br>

######Titles:

# Title1
## Title2
### Title3
#### Title4
##### Title5
###### Title6

###### Different titles:

Title1
=

Title2
-

###### Unordered lists:

  - A
  - B
  - C

<br>
###### Ordered lists:

  3. A
  2. B
  1. C

<br>
###### Nested Unordered lists:

  - A
  - B
    - B1
    - B2
  - C

<br>
###### Nested Ordered lists:

  3. A
  2. B
    2. B1
    1. B2
  1. C

<br>
###### Nested Mixed lists:
<br>(4 spaces needed)<br>


  - A
  - B
    3. B1
    2. B2
        - B21
        - B22
            2. B221
            1. B222
        - B23
    1. B3
  - C

<br>
###### Bold text:

I'm **bold**.<br>
I'm __bold__.

<br>
###### Italic text:

I'm *italic*.<br>
I'm _italic_.

<br>
###### Bold Italic text:

I'm ***bold and italic***.<br>
I'm ___bold and italic___.<br>

<br>
###### Code blocks:

```
Beep-boop I am a robot,
so I talk in monospace.

```

<br>
###### Quote blocks:


>Someone said.
>>Answering this.
>>>That was an answer to this.

>>Then the previous continued.

>And then the first one.

<br>
###### Images:

Local image:<br>

![](./Logo.png)

<br>Online image:<br>

![](https://codeberg.org/repo-avatars/30238-d7f099cee5b3b6d61602e72024a699a8)

<br>
###### Links:

With placeholder text:<br>
[I am a link](https://codeberg.org/MorsMortium/LBRY-GTK)

<br>Without placeholder text:<br>
[](https://codeberg.org/MorsMortium/LBRY-GTK)

<br>Image links:<br>
[![](https://codeberg.org/repo-avatars/30238-d7f099cee5b3b6d61602e72024a699a8)](https://codeberg.org/MorsMortium/LBRY-GTK)

<br>You can go back to the main article [here](./Index.md).
