#####################################################################
#                                                                   #
#  THIS IS A SOURCE CODE FILE FROM A PROGRAM TO INTERACT WITH THE   #
# LBRY PROTOCOL ( lbry.com ). IT WILL USE THE LBRY SDK ( lbrynet )  #
# FROM THEIR REPOSITORY ( https://github.com/lbryio/lbry-sdk )      #
# WHICH I GONNA PRESENT TO YOU AS A BINARY. SINCE I DID NOT DEVELOP #
# IT AND I'M LAZY TO INTEGRATE IN A MORE SMART WAY. THE SOURCE CODE #
# OF THE SDK IS AVAILABLE IN THE REPOSITORY MENTIONED ABOVE.        #
#                                                                   #
#      ALL THE CODE IN THIS REPOSITORY INCLUDING THIS FILE IS       #
# (C) J.Y.Amihud and Other Contributors 2021. EXCEPT THE LBRY SDK.  #
# YOU CAN USE THIS FILE AND ANY OTHER FILE IN THIS REPOSITORY UNDER #
# THE TERMS OF GNU GENERAL PUBLIC LICENSE VERSION 3 OR ANY LATER    #
# VERSION. TO FIND THE FULL TEXT OF THE LICENSE GO TO THE GNU.ORG   #
# WEBSITE AT ( https://www.gnu.org/licenses/gpl-3.0.html ).         #
#                                                                   #
# THE LBRY SDK IS UNFORTUNATELY UNDER THE MIT LICENSE. IF YOU ARE   #
# NOT INTENDING TO USE MY CODE AND JUST THE SDK. YOU CAN FIND IT ON #
# THEIR OFFICIAL REPOSITORY ABOVE. THEIR LICENSE CHOICE DOES NOT    #
# SPREAD ONTO THIS PROJECT. DON'T GET A FALSE ASSUMPTION THAT SINCE #
# THEY USE A PUSH-OVER LICENSE, I GONNA DO THE SAME. I'M NOT.       #
#                                                                   #
# THE LICENSE CHOSEN FOR THIS PROJECT WILL PROTECT THE 4 ESSENTIAL  #
# FREEDOMS OF THE USER FURTHER, BY NOT ALLOWING ANY WHO TO CHANGE   #
# THE LICENSE AT WILL. SO NO PROPRIETARY SOFTWARE DEVELOPER COULD   #
# TAKE THIS CODE AND MAKE THEIR USER-SUBJUGATING SOFTWARE FROM IT.  #
#                                                                   #
#####################################################################

# This file will perform a simple search on the LBRY network.

from subprocess import *
import json, requests

from flbry import url, settings, channel, parse, error
from flbry.variables import *


def list(wallet_id="", page_size=5, page=1, server="http://localhost:5279"):

    json = {
        "method": "wallet_list",
        "params": {
            "page_size": page_size,
            "page": page,
        },
    }

    if wallet_id != "":
        json["params"]["wallet_id"] = wallet_id

    try:
        json_data = {"error": {}}
        json_data = requests.post(server, json=json).json()
        json_data = json_data["result"]
    except Exception as e:
        return error.error(e, json_data["error"])

    return parse.wallets(json_data, server=server)


def history(page_size=5, page=1, server="http://localhost:5279"):

    # This function will output wallet history.

    try:
        json_data = {"error": {}}
        json_data = requests.post(
            server,
            json={
                "method": "transaction_list",
                "params": {"page_size": page_size, "page": page},
            },
        ).json()
        json_data = json_data["result"]
    except Exception as e:
        return error.error(e, json_data["error"])

    return parse.balances(json_data, server=server)


def balance(server="http://localhost:5279"):

    # Prints all wallet balance information

    try:
        json_data = {"error": {}}
        json_data = requests.post(
            server, json={"method": "wallet_balance"}
        ).json()
        json_data = json_data["result"]
    except Exception as e:
        return error.error(e, json_data["error"])

    return json_data


# def create(
#    claim_id, amount, tip=False, channel_id="", channel_name="",
#    channel_account_id="", account_id="", wallet_id="", funding_account_ids=[],
#    comment="", preview=False, blocking=False):


def support(claim_id, amount="", tip=False):
    # Dialog to send support or tip to claim_id

    data_print = {
        "categories": ["Amount", "Channel", "Comment"],
        "size": [1, 3, 5],
        "data": [],
    }
    amount = float(settings.get("default_tip"))
    channel_name = "[anonymous]"
    channel_id = ""
    comment = None

    complete(["amount", "channel", "send", "comment"])

    if tip:
        support_strs = ["Tipped", "tip"]
    else:
        support_strs = ["Boosted", "boost"]

    while True:
        # Just setting data directly wasn't working, so we clear it then append to it
        data_print["data"] = []
        # f'amount:.5f}' shows the amount in decimal notation with 5 places after the decimal
        data_print["data"].append([f"{amount:.5f}", channel_name, str(comment)])

        table(data_print, False)
        center("---type 'help' for support commands---")

        c = input(typing_dots())
        if not c:
            break

        if c == "amount":
            am = ""
            # while am is not a float
            while type(am) != type(5.0):
                am = input("   Amount: ")

                # If the user types nothing just keep the amount the same
                if not am:
                    break

                try:
                    amount = float(am)
                    am = amount
                except:
                    pass

        elif c == "channel":
            channel_name, channel_id = channel.select(
                "Select the signing channel.", claim_id=True
            )

        elif c == "help":
            markdown.draw("help/support.md", "Support Help")

        elif c.startswith("comment"):
            c = c + " "
            a = c[c.find(" ") :]
            if len(a) > 1:
                comment = file_or_editor(
                    a,
                    "Type the comment here. Don't forget to save. Then return to FastLBRY.",
                )
            else:
                comment = input("   Comment: ")

        elif c == "send":
            args = [
                lbrynet_binary["b"],
                "support",
                "create",
                "--claim_id=" + claim_id,
                "--amount=" + f"{amount:.5f}",
            ]

            if channel_id:
                args.append("--channel_id=" + channel_id)

            if comment:
                args.append("--comment=" + comment)

            if tip:
                args.append("--tip")

            try:
                x = check_output(args)
                center(
                    support_strs[0] + " with " + f"{amount:.5f}" + " LBC",
                    "bdgr",
                )
            except Exception as e:
                center(
                    "Error sending " + support_strs[1] + ": " + str(e), "bdrd"
                )

            break


def addresses():
    w, h = tsize()
    page_size = h - 5
    # TODO: At the time of writing, pagination in this command is not working, so we only get the first page.
    # When this gets fixed, please add pagination here.
    out = check_output(
        [
            lbrynet_binary["b"],
            "address",
            "list",
            "--page_size=" + str(page_size),
        ]
    )

    out = json.loads(out)

    try:

        data_print = {"categories": ["Address"], "size": [1], "data": []}

        # List what we found
        for n, i in enumerate(out["items"]):
            data_print["data"].append([i["address"]])

        table(data_print)
        center("")

    # Error messages
    except Exception as e:
        pass
        """
        if "code" in out:
            print("    Error code: ", out["code"])
            if out["code"] == -32500:
                print("   SDK is still starting. Patience!")
        else:
            print("    Error :", e)
        return
        """


def address_send(amount="", address=""):
    if not amount:
        amount = input("   Amount to send: ")

    if not address:
        address = input("   Address to send to: ")

    out = check_output(
        [lbrynet_binary["b"], "wallet", "send", str(amount), address]
    )

    out = json.loads(out)

    if "message" in out:
        center("   Error sending to address: " + out["message"], "bdrd")
    else:
        center("   Successfully sent " + str(amount) + " to " + address, "bdgr")
