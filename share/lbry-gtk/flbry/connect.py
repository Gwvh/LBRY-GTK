#####################################################################
#                                                                   #
#  THIS IS A SOURCE CODE FILE FROM A PROGRAM TO INTERACT WITH THE   #
# LBRY PROTOCOL ( lbry.com ). IT WILL USE THE LBRY SDK ( lbrynet )  #
# FROM THEIR REPOSITORY ( https://github.com/lbryio/lbry-sdk )      #
# WHICH I GONNA PRESENT TO YOU AS A BINARY. SINCE I DID NOT DEVELOP #
# IT AND I'M LAZY TO INTEGRATE IN A MORE SMART WAY. THE SOURCE CODE #
# OF THE SDK IS AVAILABLE IN THE REPOSITORY MENTIONED ABOVE.        #
#                                                                   #
#      ALL THE CODE IN THIS REPOSITORY INCLUDING THIS FILE IS       #
# (C) J.Y.Amihud and Other Contributors 2021. EXCEPT THE LBRY SDK.  #
# YOU CAN USE THIS FILE AND ANY OTHER FILE IN THIS REPOSITORY UNDER #
# THE TERMS OF GNU GENERAL PUBLIC LICENSE VERSION 3 OR ANY LATER    #
# VERSION. TO FIND THE FULL TEXT OF THE LICENSE GO TO THE GNU.ORG   #
# WEBSITE AT ( https://www.gnu.org/licenses/gpl-3.0.html ).         #
#                                                                   #
# THE LBRY SDK IS UNFORTUNATELY UNDER THE MIT LICENSE. IF YOU ARE   #
# NOT INTENDING TO USE MY CODE AND JUST THE SDK. YOU CAN FIND IT ON #
# THEIR OFFICIAL REPOSITORY ABOVE. THEIR LICENSE CHOICE DOES NOT    #
# SPREAD ONTO THIS PROJECT. DON'T GET A FALSE ASSUMPTION THAT SINCE #
# THEY USE A PUSH-OVER LICENSE, I GONNA DO THE SAME. I'M NOT.       #
#                                                                   #
# THE LICENSE CHOSEN FOR THIS PROJECT WILL PROTECT THE 4 ESSENTIAL  #
# FREEDOMS OF THE USER FURTHER, BY NOT ALLOWING ANY WHO TO CHANGE   #
# THE LICENSE AT WILL. SO NO PROPRIETARY SOFTWARE DEVELOPER COULD   #
# TAKE THIS CODE AND MAKE THEIR USER-SUBJUGATING SOFTWARE FROM IT.  #
#                                                                   #
#####################################################################

# This file will start the lbrynet sdk.

import subprocess, requests, time, queue
from flbry.variables import *
from flbry import error


def start(
    function=print,
    exit_queue=queue.Queue(),
    binary="lbrynet start",
    timeout=30,
    server="http://localhost:5279",
):
    if check("", server):
        function(status(server))
    else:
        subprocess.Popen(
            binary,
            stdout=subprocess.DEVNULL,
            stderr=subprocess.STDOUT,
            shell=True,
        )

    slept = 0

    while True:
        if check(function, server):
            break
        exit = False
        try:
            exit = exit_queue.get(block=False)
        except:
            pass
        if exit or timeout < slept:
            break
        sleep = 0.5
        time.sleep(sleep)
        slept += sleep

    return check("", server)


def stop(server="http://localhost:5279"):
    try:
        requests.post(server, json={"method": "stop"})
    except:
        pass
    if check("", server):
        return "LBRY Connection Closed"
    else:
        return "SDK is not running"


def status(server="http://localhost:5279"):
    try:
        return requests.post(server, json={"method": "status"}).json()[
            "result"
        ]["startup_status"]
    except:
        return {"blob_manager": False}


def check(function="", server="http://localhost:5279"):
    # This output true or false
    # whether the SDK is running

    gotstatus = status(server)

    started = 0
    for key in gotstatus.keys():
        if gotstatus[key]:
            started += 1

    if function != "":
        function(gotstatus)

    if started == len(gotstatus.keys()):
        return True

    return False


def check_timeout(function="", timeout=30, server="http://localhost:5279"):
    slept = 0

    while True:
        if check(function, server):
            break
        exit = False
        try:
            exit = exit_queue.get(block=False)
        except:
            pass
        if exit or timeout < slept:
            break
        sleep = 0.5
        time.sleep(sleep)
        slept += sleep

    return check("", server)
