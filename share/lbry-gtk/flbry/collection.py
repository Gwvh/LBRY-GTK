#####################################################################
#                                                                   #
#  THIS IS A SOURCE CODE FILE FROM A PROGRAM TO INTERACT WITH THE   #
# LBRY PROTOCOL ( lbry.com ). IT WILL USE THE LBRY SDK ( lbrynet )  #
# FROM THEIR REPOSITORY ( https://github.com/lbryio/lbry-sdk )      #
# WHICH I GONNA PRESENT TO YOU AS A BINARY. SINCE I DID NOT DEVELOP #
# IT AND I'M LAZY TO INTEGRATE IN A MORE SMART WAY. THE SOURCE CODE #
# OF THE SDK IS AVAILABLE IN THE REPOSITORY MENTIONED ABOVE.        #
#                                                                   #
#      ALL THE CODE IN THIS REPOSITORY INCLUDING THIS FILE IS       #
# (C) J.Y.Amihud and Other Contributors 2021. EXCEPT THE LBRY SDK.  #
# YOU CAN USE THIS FILE AND ANY OTHER FILE IN THIS REPOSITORY UNDER #
# THE TERMS OF GNU GENERAL PUBLIC LICENSE VERSION 3 OR ANY LATER    #
# VERSION. TO FIND THE FULL TEXT OF THE LICENSE GO TO THE GNU.ORG   #
# WEBSITE AT ( https://www.gnu.org/licenses/gpl-3.0.html ).         #
#                                                                   #
# THE LBRY SDK IS UNFORTUNATELY UNDER THE MIT LICENSE. IF YOU ARE   #
# NOT INTENDING TO USE MY CODE AND JUST THE SDK. YOU CAN FIND IT ON #
# THEIR OFFICIAL REPOSITORY ABOVE. THEIR LICENSE CHOICE DOES NOT    #
# SPREAD ONTO THIS PROJECT. DON'T GET A FALSE ASSUMPTION THAT SINCE #
# THEY USE A PUSH-OVER LICENSE, I GONNA DO THE SAME. I'M NOT.       #
#                                                                   #
# THE LICENSE CHOSEN FOR THIS PROJECT WILL PROTECT THE 4 ESSENTIAL  #
# FREEDOMS OF THE USER FURTHER, BY NOT ALLOWING ANY WHO TO CHANGE   #
# THE LICENSE AT WILL. SO NO PROPRIETARY SOFTWARE DEVELOPER COULD   #
# TAKE THIS CODE AND MAKE THEIR USER-SUBJUGATING SOFTWARE FROM IT.  #
#                                                                   #
#####################################################################

import requests

from flbry import parse, error


def list(
    resolve=False,
    resolve_claims=False,
    account_id="",
    wallet_id="",
    page_size=5,
    page=1,
    server="http://localhost:5279",
):

    json = {
        "method": "collection_list",
        "params": {"page_size": page_size, "page": page},
    }

    if resolve_claims != False:
        json["params"]["resolve_claims"] = resolve_claims

    if account_id != "":
        json["params"]["account_id"] = account_id

    if wallet_id != "":
        json["params"]["wallet_id"] = wallet_id

    if resolve != False:
        json["params"]["resolve"] = resolve

    try:
        json_data = {"error": {}}
        json_data = requests.post(server, json=json).json()
        json_data = json_data["result"]
    except Exception as e:
        return error.error(e, json_data["error"])

    return parse.publications(json_data, server=server)


def resolve(
    claim_id="",
    url="",
    wallet_id="",
    page_size=5,
    page=1,
    server="http://localhost:5279",
):

    json = {
        "method": "collection_resolve",
        "params": {"page_size": page_size, "page": page},
    }

    if wallet_id != "":
        json["params"]["wallet_id"] = wallet_id

    if claim_id != "":
        json["params"]["claim_id"] = claim_id

    if url != "":
        json["params"]["url"] = url

    try:
        json_data = {"error": {}}
        json_data = requests.post(server, json=json).json()
        json_data = json_data["result"]
    except Exception as e:
        return error.error(e, json_data["error"])

    return parse.publications(json_data, server=server)
