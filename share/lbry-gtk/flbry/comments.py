#####################################################################
#                                                                   #
#  THIS IS A SOURCE CODE FILE FROM A PROGRAM TO INTERACT WITH THE   #
# LBRY PROTOCOL ( lbry.com ). IT WILL USE THE LBRY SDK ( lbrynet )  #
# FROM THEIR REPOSITORY ( https://github.com/lbryio/lbry-sdk )      #
# WHICH I GONNA PRESENT TO YOU AS A BINARY. SINCE I DID NOT DEVELOP #
# IT AND I'M LAZY TO INTEGRATE IN A MORE SMART WAY. THE SOURCE CODE #
# OF THE SDK IS AVAILABLE IN THE REPOSITORY MENTIONED ABOVE.        #
#                                                                   #
#      ALL THE CODE IN THIS REPOSITORY INCLUDING THIS FILE IS       #
# (C) J.Y.Amihud and Other Contributors 2021. EXCEPT THE LBRY SDK.  #
# YOU CAN USE THIS FILE AND ANY OTHER FILE IN THIS REPOSITORY UNDER #
# THE TERMS OF GNU GENERAL PUBLIC LICENSE VERSION 3 OR ANY LATER    #
# VERSION. TO FIND THE FULL TEXT OF THE LICENSE GO TO THE GNU.ORG   #
# WEBSITE AT ( https://www.gnu.org/licenses/gpl-3.0.html ).         #
#                                                                   #
# THE LBRY SDK IS UNFORTUNATELY UNDER THE MIT LICENSE. IF YOU ARE   #
# NOT INTENDING TO USE MY CODE AND JUST THE SDK. YOU CAN FIND IT ON #
# THEIR OFFICIAL REPOSITORY ABOVE. THEIR LICENSE CHOICE DOES NOT    #
# SPREAD ONTO THIS PROJECT. DON'T GET A FALSE ASSUMPTION THAT SINCE #
# THEY USE A PUSH-OVER LICENSE, I GONNA DO THE SAME. I'M NOT.       #
#                                                                   #
# THE LICENSE CHOSEN FOR THIS PROJECT WILL PROTECT THE 4 ESSENTIAL  #
# FREEDOMS OF THE USER FURTHER, BY NOT ALLOWING ANY WHO TO CHANGE   #
# THE LICENSE AT WILL. SO NO PROPRIETARY SOFTWARE DEVELOPER COULD   #
# TAKE THIS CODE AND MAKE THEIR USER-SUBJUGATING SOFTWARE FROM IT.  #
#                                                                   #
#####################################################################

# This file will perform a simple search on the LBRY network.

import json, requests

from flbry import url, channel, parse, settings, error
from flbry.variables import *


def sign(channel_name, data, server="http://localhost:5279"):
    try:
        json_data = {"error": {}}
        json_data = requests.post(
            server,
            json={
                "method": "channel_sign",
                "params": {
                    "channel_name": channel_name,
                    "hexdata": data.encode("utf-8").hex(),
                },
            },
        ).json()
        json_data = json_data["result"]
    except Exception as e:
        return error.error(e, json_data["error"])

    return [json_data["signature"], json_data["signing_ts"]]


def setting_get(channel_id, comment_server):
    try:
        json_data = {"error": {}}
        json_data = requests.post(
            comment_server + "?m=reaction.React",
            json={
                "id": 1,
                "jsonrpc": "2.0",
                "method": "setting.Get",
                "params": {"channel_id": channel_id},
            },
        ).json()
        json_data = json_data["result"]
    except Exception as e:
        return error.error(e, json_data["error"])

    return json_data


def reaction_react(
    channel_name,
    channel_id,
    comment_server,
    comment_ids,
    reaction_type,
    remove=False,
    server="http://localhost:5279",
):
    signage = sign(channel_name, channel_name, server)

    if isinstance(signage, str):
        return signage

    signature, signing_ts = signage

    clear_types = ""

    if not remove:
        if reaction_type == "like":
            clear_types = "dislike"
        elif reaction_type == "dislike":
            clear_types = "like"

    json = {
        "id": 1,
        "jsonrpc": "2.0",
        "method": "reaction.React",
        "params": {
            "channel_name": channel_name,
            "channel_id": channel_id,
            "comment_ids": comment_ids,
            "type": reaction_type,
            "signature": signature,
            "signing_ts": signing_ts,
        },
    }

    if remove:
        json["params"]["remove"] = remove
    elif clear_types != "":
        json["params"]["clear_types"] = clear_types

    try:
        json_data = {"error": {}}
        json_data = requests.post(
            comment_server + "?m=reaction.React", json=json
        ).json()
        json_data = json_data["result"]
    except Exception as e:
        return error.error(e, json_data["error"])

    return json_data


def reaction_list(
    channel_name,
    channel_id,
    comment_server,
    comment_ids,
    server="http://localhost:5279",
):

    signage = sign(channel_name, channel_name, server)

    signature, signing_ts = "", ""

    if not isinstance(signage, str):
        signature, signing_ts = signage

    try:
        json_data = {"error": {}}
        json_data = requests.post(
            comment_server + "?m=reaction.List",
            json={
                "id": 1,
                "jsonrpc": "2.0",
                "method": "reaction.List",
                "params": {
                    "channel_name": channel_name,
                    "channel_id": channel_id,
                    "comment_ids": comment_ids,
                    "signature": signature,
                    "signing_ts": signing_ts,
                },
            },
        ).json()
        json_data = json_data["result"]
    except Exception as e:
        return error.error(e, json_data["error"])

    return json_data


def list(
    claim_id,
    comment_server,
    channel_name="",
    channel_id="",
    parent_id="",
    page_size=5,
    page=1,
    server="http://localhost:5279",
):

    # This function will list a list of comments of a certain claim_id
    # It will preview a very basic form of comment. You will have
    # to select a comment to interact with it further. Like read the
    # whole text, if it not short ( or in a markdown format ). Or
    # do other things like replies.

    json = {
        "id": 1,
        "jsonrpc": "2.0",
        "method": "comment.List",
        "params": {
            "claim_id": claim_id,
            "page_size": page_size,
            "page": page,
            "sort_by": 3,
            "top_level": True,
        },
    }

    if channel_name != "":
        json["params"]["channel_name"] = channel_name

    if channel_id != "":
        json["params"]["channel_id"] = channel_id

    if parent_id != "":
        json["params"]["parent_id"] = parent_id
        json["params"]["top_level"] = False

    try:
        json_data = {"error": {}}
        json_data = requests.post(
            comment_server + "?m=comment.List", json=json
        ).json()
        json_data = json_data["result"]
    except Exception as e:
        return error.error(e, json_data["error"])

    return [parse.comments(json_data, server=server), json_data["total_pages"]]


def post(
    claim_id,
    comment,
    channel_name,
    channel_id,
    comment_server,
    parent_id="",
    support_tx_id="",
    server="http://localhost:5279",
):

    # This will post a comment under either a publication or a
    # comment as a reply.

    signage = sign(channel_name, comment, server)

    if isinstance(signage, str):
        return signage

    signature, signing_ts = signage

    json = {
        "id": 1,
        "jsonrpc": "2.0",
        "method": "comment.Create",
        "params": {
            "comment": comment,
            "channel_id": channel_id,
            "channel_name": channel_name,
            "claim_id": claim_id,
            "signature": signature,
            "signing_ts": signing_ts,
        },
    }

    if parent_id != "":
        json["params"]["parent_id"] = parent_id

    if support_tx_id != "":
        json["params"]["support_tx_id"] = support_tx_id

    try:
        json_data = {"error": {}}
        json_data = requests.post(
            comment_server + "?m=comment.Create", json=json
        ).json()
        json_data = json_data["result"]
    except Exception as e:
        return error.error(e, json_data["error"])

    return json_data


"""
def inbox(opt=10):

    # This function will return the latest comments from the latest
    # publications. Similar to an email inbox. But with a limitation.

    # There is no system in the SDK to implement a history of comments
    # seamlessly. So then I need to cash a large file of comments. Or
    # do something clever. I think there will be a few options.

    # You noticed the opt=10 preset on the top. It's the default value.
    # Basically the user might type one of 4 things.

    #    inbox
    #    inbox 40     (or any number what so ever)
    #    inbox all
    #    inbox cashed

    # Each will run a slightly different algorithm to get the inbox
    # comments.

    #    inbox

    # This will use the predefined 10 and read last 10 publications
    # comments to add. It will combine them with the pre-cashed ones
    # for the user to view. As you may imagine, giving it a number as
    # in:

    #    inbox 40
    #    inbox 2
    #    inbox 50

    # Will load this number of publications. To update with them the
    # cash and then present it to the user.

    #    inbox all

    # This one will take longest. But might be useful for some users.
    # This will go through all publications and cash comments from all
    # of them.

    #    inbox cashed

    # This one is the fastest of them. It will only read the cash file
    # and present it to the user. So for instance you want to quickly
    # go back to the inbox without loading anything at all.


    try:
        opt = int(opt)
        reached = opt
        goal = opt
    except:
        goal = 0
        if opt == "all":
            reached = True
        else:
            reached = False

    # Updating the cash file ( inbox.json )
    page = 0
    items_total = 0
    current_item = 0



    try:
        with open(settings.get_settings_folder()+'inbox.json') as json_file:
            comments_cache = json.load(json_file)
    except:
        comments_cache = []

    checked_publications = []

    while reached > 0:

        if type(reached) == int:
            reached = reached - 50

        page = page + 1
        page_size = 50

        # Getting data about publications.


        if page != 1:
            out = check_output([lbrynet_binary["b"],
                         "stream", "list",
                         '--page='+str(page),
                         '--page_size='+str(page_size),
                            "--no_totals"])
            out2 = check_output([lbrynet_binary["b"],
                         "channel", "list",
                         '--page='+str(page),
                         '--page_size='+str(page_size),
                            "--no_totals"])
        else:
            out = check_output([lbrynet_binary["b"],
                         "stream", "list",
                         '--page='+str(page),
                                '--page_size='+str(page_size)])
            out2 = check_output([lbrynet_binary["b"],
                         "channel", "list",
                         '--page='+str(page),
                                '--page_size='+str(page_size)])



        # Now we want to parse the json
        items = []
        try:
            out = json.loads(out)
            out2 = json.loads(out2)
            items = out["items"]
            try:
                items = items[:int(opt)]
            except:
                pass
            for i in out2["items"]:
                items.append(i)
        except:
            break

        if not items:
            break

        if page == 1:
            # Getting Totals to calculate the progress bar
            if reached == True:
                items_total = out["total_items"] + out2["total_items"]
            else:
                try:
                    items_total = int(opt) + out2["total_items"]
                except:
                    items_total = 0

        # Reading items from the items

        for publication in items:

            # skip dublicate publications. ( like when you edited
            # a publication )

            if publication["name"] in checked_publications:
                continue
            checked_publications.append(publication["name"])

            current_item = current_item + 1

            # If above the requested amount.
            #if current_item > items_total:
                #break

            # Draw progress bar
            progress_bar(current_item, items_total, publication["name"])

            # let's now get all the comments
            claim_id = publication["claim_id"]

            comment_page = 0

            while True:

                comment_page = comment_page + 1

                cout = check_output([lbrynet_binary["b"],
                        "comment", "list", '--claim_id='+claim_id,
                         '--page='+str(comment_page),
                            '--page_size='+str(50),
                            '--include_replies'])

                try:
                    cout = json.loads(cout)
                except:
                    break

                # TODO: For now I'm stopping on first page when ever I'm
                #       loading channel's comments ( community disscussion ).
                #       This is obviously not going to work with "inbox all",
                #       so please make the logic a bit smarter, so it will work.

                if "items" not in cout or publication["value_type"] == "channel":
                    break

                for i in cout["items"]:

                    # I want to add a few things into the comment data
                    i["publication_url"] = publication["permanent_url"]
                    i["publication_name"] = publication["name"]
                    try:
                        i["publication_title"] =  publication["value"]["title"]
                    except:
                        i["publication_title"] =  publication["name"]

                    # TODO: It could get doubles into the cache when some
                    #       body replies to a comment. Since i will now not
                    #       equal the almost identical item in comments_cache.
                    #       Please come up with some smarter way to get rid of
                    #       doubles.
                    if i not in comments_cache:
                        comments_cache.append(i)

    # Let's sort the comments based on the time they were sent
    comments_cache = sorted(comments_cache, key=lambda k: k['timestamp'], reverse=True)

    with open(settings.get_settings_folder()+'inbox.json', 'w') as fp:
            json.dump(comments_cache, fp , indent=4)



    # Now that we have comments cached and ready. I can start actually showing
    # them.

    w, h = tsize()
    page_size = (h-5)
    page = 0

    while True:

        data_print = {"categories":["Tip LBC", "Comments", "Publication",  "Channel",  "Preview"],
                          "size":[1,1,4,2,4],
                          "data":[]}

        items = []

        for n, i in enumerate(comments_cache):

            startfrom = int( page * page_size )
            endat     = int( startfrom + page_size )

            if n in range(startfrom, endat):

                items.append(i)

                preview = "---!Failed Loading comment---"
                support = 0
                replies = 0
                where = "[some publication]"
                bywho = "[anonymous]"

                try:
                    comment = i["comment"]
                    preview = comment.replace("\n", " ")
                    where = i["publication_title"]
                    support = i["support_amount"]
                    bywho = i["channel_name"]
                    replies = i["replies"]

                except:
                    pass

                data_print["data"].append([support, replies, where, bywho, preview])
        table(data_print)

        # Tell the user that they might want to load more
        center("---type 'more' to load more---")


        # Making sure that we stop every time a new page is reached
        c =  input(typing_dots())
        if c == "more":
            page = page +1
            continue

        try:
            c = int(c)
        except:
            return

        view(items[c])
        c = input(typing_dots())
"""


def update(
    comment,
    comment_id,
    channel_name,
    channel_id,
    comment_server,
    server="http://localhost:5279",
):

    signage = sign(channel_name, comment, server)

    if isinstance(signage, str):
        return signage

    signature, signing_ts = signage

    try:
        json_data = {"error": {}}
        json_data = requests.post(
            comment_server + "?m=comment.Edit",
            json={
                "id": 1,
                "jsonrpc": "2.0",
                "method": "comment.Edit",
                "params": {
                    "comment_id": comment_id,
                    "comment": comment,
                    "channel_name": channel_name,
                    "channel_id": channel_id,
                    "signature": signature,
                    "signing_ts": signing_ts,
                },
            },
        ).json()
        json_data = json_data["result"]
    except Exception as e:
        return error.error(e, json_data["error"])

    return json_data


def delete(
    comment_id,
    channel_name,
    channel_id,
    comment_server,
    server="http://localhost:5279",
):
    signage = sign(channel_name, comment_id, server)

    if isinstance(signage, str):
        return signage

    signature, signing_ts = signage

    try:
        json_data = {"error": {}}
        json_data = requests.post(
            comment_server + "?m=comment.Abandon",
            json={
                "id": 1,
                "jsonrpc": "2.0",
                "method": "comment.Abandon",
                "params": {
                    "channel_name": channel_name,
                    "channel_id": channel_id,
                    "comment_id": comment_id,
                    "signature": signature,
                    "signing_ts": signing_ts,
                },
            },
        ).json()
        json_data = json_data["result"]
    except Exception as e:
        return error.error(e, json_data["error"])

    return json_data
