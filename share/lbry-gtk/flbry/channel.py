#####################################################################
#                                                                   #
#  THIS IS A SOURCE CODE FILE FROM A PROGRAM TO INTERACT WITH THE   #
# LBRY PROTOCOL ( lbry.com ). IT WILL USE THE LBRY SDK ( lbrynet )  #
# FROM THEIR REPOSITORY ( https://github.com/lbryio/lbry-sdk )      #
# WHICH I GONNA PRESENT TO YOU AS A BINARY. SINCE I DID NOT DEVELOP #
# IT AND I'M LAZY TO INTEGRATE IN A MORE SMART WAY. THE SOURCE CODE #
# OF THE SDK IS AVAILABLE IN THE REPOSITORY MENTIONED ABOVE.        #
#                                                                   #
#      ALL THE CODE IN THIS REPOSITORY INCLUDING THIS FILE IS       #
# (C) J.Y.Amihud and Other Contributors 2021. EXCEPT THE LBRY SDK.  #
# YOU CAN USE THIS FILE AND ANY OTHER FILE IN THIS REPOSITORY UNDER #
# THE TERMS OF GNU GENERAL PUBLIC LICENSE VERSION 3 OR ANY LATER    #
# VERSION. TO FIND THE FULL TEXT OF THE LICENSE GO TO THE GNU.ORG   #
# WEBSITE AT ( https://www.gnu.org/licenses/gpl-3.0.html ).         #
#                                                                   #
# THE LBRY SDK IS UNFORTUNATELY UNDER THE MIT LICENSE. IF YOU ARE   #
# NOT INTENDING TO USE MY CODE AND JUST THE SDK. YOU CAN FIND IT ON #
# THEIR OFFICIAL REPOSITORY ABOVE. THEIR LICENSE CHOICE DOES NOT    #
# SPREAD ONTO THIS PROJECT. DON'T GET A FALSE ASSUMPTION THAT SINCE #
# THEY USE A PUSH-OVER LICENSE, I GONNA DO THE SAME. I'M NOT.       #
#                                                                   #
# THE LICENSE CHOSEN FOR THIS PROJECT WILL PROTECT THE 4 ESSENTIAL  #
# FREEDOMS OF THE USER FURTHER, BY NOT ALLOWING ANY WHO TO CHANGE   #
# THE LICENSE AT WILL. SO NO PROPRIETARY SOFTWARE DEVELOPER COULD   #
# TAKE THIS CODE AND MAKE THEIR USER-SUBJUGATING SOFTWARE FROM IT.  #
#                                                                   #
#####################################################################

# This file will perform a simple search on the LBRY network.

from subprocess import *
import json, requests

from flbry import wallet, settings, search, parse, error
from flbry.variables import *

"""
            elif c.startswith("boost"):
                if " " in c:
                    wallet.support(signing_channel["claim_id"], amount=c[c.find(" ")+1:])
                else:
                    wallet.support(signing_channel["claim_id"])

                reshow = True

            elif c.startswith("tip"):
                if " " in c:
                    wallet.support(signing_channel["claim_id"], amount=c[c.find(" ")+1:], tip=True)
                else:
                    wallet.support(signing_channel["claim_id"], tip=True)

                reshow = True

            elif c.startswith("search"):
                channel = signing_channel["permanent_url"]

                if " " in c:
                    search.simple(c[c.find(" ")+1:], channel)
                else:
                    search.simple(channel=channel)

                reshow = True


            else:
                break
"""


def create(name=""):
    """Creates a new channel on the LBRY network"""

    # Get the name for the channel, since it's required.
    # If the user just presses enter without typing anything, it will prompt again.
    while not name:
        name = input("   Name: ")

    if not name.startswith("@"):
        name = "@" + name

    # This is the data dictionary we will use
    data = {"bid": 0.001, "name": name}

    complete(
        [
            "name",
            "bid",
            "title",
            "description",
            "email",
            "website",
            "thumbnail",
            "cover",
            "tags",
            "languages",
            "help",
            "save",
            "load",
            "create",
        ]
    )

    while True:
        show_data(data)

        c = input(typing_dots())
        if not c:
            return

        elif c.startswith("name"):
            if " " in c:
                name = c[c.find(" ") + 1 :]
            else:
                name = input("   Name: ")

            if not name.startswith("@"):
                name = "@" + name

            data["name"] = name

        elif c.startswith("bid"):
            # Get the bid
            if " " in c:
                bid = c[c.find(" ") + 1 :]
            else:
                bid = input("   Bid: ")

            # Try to convert it a float
            try:
                bid = float(bid)
            except:
                pass

            # while bid is not a float, repeat until it is
            while type(bid) != type(10.0):
                center("Bid is not a number, try again", "bdrd")
                bid = input("   Bid: ")
                try:
                    bid = float(bid)
                except:
                    pass

            data["bid"] = bid

        elif c.startswith("title"):
            if " " in c:
                title = c[c.find(" ") + 1 :]
            else:
                title = input("   Title: ")

            data["title"] = title

        elif c.startswith("description"):
            c = c + " "
            a = c[c.find(" ") :]
            if len(a) > 1:
                description = file_or_editor(
                    a,
                    "Type the description here. Don't forget to save. Then return to FastLBRY.",
                )
            else:
                description = input("   Description: ")

            data["description"] = description

        elif c.startswith("email"):
            if " " in c:
                email = c[c.find(" ") + 1 :]
            else:
                email = input("   Email: ")

            data["email"] = email

        elif c.startswith("website"):
            if " " in c:
                web_url = c[c.find(" ") + 1 :]
            else:
                web_url = input("   Website URL: ")

            data["website_url"] = web_url

        elif c.startswith("thumbnail"):
            if " " in c:
                thumb_url = c[c.find(" ") + 1 :]
            else:
                thumb_url = input("   Thumbnail URL: ")

            data["thumbnail_url"] = thumb_url

        elif c.startswith("cover"):
            if " " in c:
                cover_url = c[c.find(" ") + 1 :]
            else:
                cover_url = input("   Cover Image URL: ")

            data["cover_url"] = cover_url

        elif c.startswith("tags"):
            if " " in c:
                tags = c[c.find(" ") + 1 :]
            else:
                tags = input(
                    "   Enter the tags for the channel, separated by commas: "
                )

            tags = tags.split(",")
            # Stip each tag, so if the user types "tag1, tag2, tag3"
            # Resulting list would be: ["tag1", "tag2", "tag3"]
            for n, tag in enumerate(tags):
                tags[n] = tag.strip()

            data["tags"] = tags

        elif c.startswith("languages"):
            if " " in c:
                langs = c[c.find(" ") + 1 :]
            else:
                langs = input(
                    "   Enter the languages for the channel, separated by commas: "
                )

            langs = langs.split(",")
            for n, lang in enumerate(langs):
                langs[n] = lang.strip()

            data["languages"] = langs

        elif c == "help":
            markdown.draw("help/create-channel.md", "Create Channel Help")

        elif c.startswith("save"):
            if " " in c:
                pn = c[c.find(" ") + 1 :]
            else:
                pn = input("   Preset Name: ")

            # Create the preset folder is it's not there
            try:
                os.makedirs(settings.get_settings_folder() + "presets/channel")
            except:
                pass

            # Write the json file
            with open(
                settings.get_settings_folder()
                + "presets/channel/"
                + pn
                + ".json",
                "w",
            ) as f:
                json.dump(data, f, indent=4, sort_keys=True)

        elif c.startswith("load"):
            if " " in c:
                pn = c[c.find(" ") + 1 :]
            else:
                pn = input("   Preset Name: ")

            # loading the json file
            try:
                name = data["name"]

                with open(
                    settings.get_settings_folder()
                    + "presets/channel/"
                    + pn
                    + ".json"
                ) as f:
                    data = json.load(f)

                data["name"] = name

            except:
                center("There is no '" + pn + "' preset!", "bdrd")

        elif c == "create":
            command = [
                lbrynet_binary["b"],
                "channel",
                "create",
                "--name=" + data["name"],
                "--bid=" + str(data["bid"]),
            ]

            for i in [
                "title",
                "description",
                "email",
                "website_url",
                "thumbnail_urL",
                "cover_url",
            ]:
                if i in data:
                    command.append("--" + i + "=" + str(data[i]))

            for i in ["tags", "languages"]:
                if i in data:
                    for j in data[i]:
                        command.append("--" + i + "=" + str(j))

            out = check_output(command)
            out = json.loads(out)

            if "message" in out:
                center("Error creating channel: " + out["message"], "bdrd")
            else:
                center("Successfully created " + name, "bdgr")

            return


def channel_list(page_size=5, page=1, server="http://localhost:5279"):

    # This fucntion will give users to select one of their channels.

    try:
        json_data = {"error": {}}
        json_data = requests.post(
            server,
            json={
                "method": "channel_list",
                "params": {
                    "page_size": page_size,
                    "page": page,
                },
            },
        ).json()
        json_data = json_data["result"]
    except Exception as e:
        return error.error(e, json_data["error"])

    return parse.channels(json_data)
