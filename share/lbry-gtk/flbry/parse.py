#####################################################################
#                                                                   #
#  THIS IS A SOURCE CODE FILE FROM A PROGRAM TO INTERACT WITH THE   #
# LBRY PROTOCOL ( lbry.com ). IT WILL USE THE LBRY SDK ( lbrynet )  #
# FROM THEIR REPOSITORY ( https://github.com/lbryio/lbry-sdk )      #
# WHICH I GONNA PRESENT TO YOU AS A BINARY. SINCE I DID NOT DEVELOP #
# IT AND I'M LAZY TO INTEGRATE IN A MORE SMART WAY. THE SOURCE CODE #
# OF THE SDK IS AVAILABLE IN THE REPOSITORY MENTIONED ABOVE.        #
#                                                                   #
#      ALL THE CODE IN THIS REPOSITORY INCLUDING THIS FILE IS       #
# (C) J.Y.Amihud and Other Contributors 2021. EXCEPT THE LBRY SDK.  #
# YOU CAN USE THIS FILE AND ANY OTHER FILE IN THIS REPOSITORY UNDER #
# THE TERMS OF GNU GENERAL PUBLIC LICENSE VERSION 3 OR ANY LATER    #
# VERSION. TO FIND THE FULL TEXT OF THE LICENSE GO TO THE GNU.ORG   #
# WEBSITE AT ( https://www.gnu.org/licenses/gpl-3.0.html ).         #
#                                                                   #
# THE LBRY SDK IS UNFORTUNATELY UNDER THE MIT LICENSE. IF YOU ARE   #
# NOT INTENDING TO USE MY CODE AND JUST THE SDK. YOU CAN FIND IT ON #
# THEIR OFFICIAL REPOSITORY ABOVE. THEIR LICENSE CHOICE DOES NOT    #
# SPREAD ONTO THIS PROJECT. DON'T GET A FALSE ASSUMPTION THAT SINCE #
# THEY USE A PUSH-OVER LICENSE, I GONNA DO THE SAME. I'M NOT.       #
#                                                                   #
# THE LICENSE CHOSEN FOR THIS PROJECT WILL PROTECT THE 4 ESSENTIAL  #
# FREEDOMS OF THE USER FURTHER, BY NOT ALLOWING ANY WHO TO CHANGE   #
# THE LICENSE AT WILL. SO NO PROPRIETARY SOFTWARE DEVELOPER COULD   #
# TAKE THIS CODE AND MAKE THEIR USER-SUBJUGATING SOFTWARE FROM IT.  #
#                                                                   #
#####################################################################

import traceback
from flbry import url, error


def single_publication(i, server="http://localhost:5279"):
    title = ""
    ftype = "claim"
    publication_url = ""
    bywho = ""
    image_url = ""
    claim_id = ""

    try:
        try:
            title = i["value"]["title"]
        except:
            title = i["name"]

        try:
            claim_id = i["claim_id"]
        except:
            pass

        try:
            ftype = i["value"]["stream_type"]
        except:
            ftype = i["value_type"]
        try:
            publication_url = i["permanent_url"]
        except:
            publication_url = i["canonical_url"]

        try:
            bywho = i["signing_channel"]["name"]
        except:
            bywho = i["name"]

        if ftype != "repost":
            try:
                image_url = i["value"]["thumbnail"]["url"]
            except:
                pass
        else:
            try:
                image_url = url.get([publication_url], server=server)[0][
                    "value"
                ]["thumbnail"]["url"]
            except:
                pass

    except:
        pass

    return [bywho, title, ftype, publication_url, image_url, claim_id]


def single_comment(i, server="http://localhost:5279"):
    comment = ""
    support = 0
    replies = 0
    bywho = ""
    reply_comment_id = ""
    channel_url = ""
    image_url = ""

    try:
        try:
            comment = i["comment"]
        except:
            pass

        try:
            support = i["support_amount"]
        except:
            pass

        try:
            bywho = i["channel_name"]
        except:
            pass

        try:
            replies = i["replies"]
        except:
            pass

        try:
            reply_comment_id = i["comment_id"]
        except:
            pass

        try:
            channel_url = i["channel_url"]
        except:
            pass

        try:

            image_url = url.get([channel_url], server=server)[0]["value"][
                "thumbnail"
            ]["url"]
        except:
            pass

    except:
        pass

    return [
        replies,
        support,
        bywho,
        comment,
        reply_comment_id,
        channel_url,
        image_url,
    ]


def single_channel(i, server="http://localhost:5279"):
    title = ""
    ftype = "claim"
    publication_url = ""
    bywho = ""
    image_url = ""
    claim_id = ""
    try:
        try:
            bywho = i["name"]
        except:
            pass

        try:
            publication_url = i["permanent_url"]
        except:
            pass

        try:
            image_url = i["value"]["thumbnail"]["url"]
        except:
            pass

        try:
            ftype = i["value_type"]
        except:
            pass

        try:
            title = i["value"]["title"]
        except:
            pass

        try:
            claim_id = i["claim_id"]
        except:
            pass

    except:
        pass

    return [bywho, title, ftype, publication_url, image_url, claim_id]


def single_balance(i, server="http://localhost:5279"):
    confirm = ""
    amount = ""
    tip = False
    publication = ""
    bywho = ""
    publication_url = ""
    image_url = ""
    ftype = ""
    claim_id = ""

    try:
        confirm = i["confirmations"]
    except:
        pass

    try:
        amount = i["value"]
    except:
        pass

    try:
        tip = i["support_info"][0]["is_tip"]
    except:
        pass

    if i["support_info"] != []:
        info = i["support_info"][0]
    elif i["update_info"] != []:
        info = i["update_info"][0]
    elif i["abandon_info"] != []:
        info = i["abandon_info"][0]

    try:
        publication = info["claim_name"]
        claim_id = info["claim_id"]
        amount = info["amount"]
    except:
        pass

    if publication != "":

        json_data = url.get([claim_id], server=server)[0]
        if isinstance(json_data, str):
            return json_data

        try:
            publication_url = json_data["canonical_url"]
        except:
            pass
        try:
            image_url = json_data["value"]["thumbnail"]["url"]
        except:
            pass

        try:
            bywho = json_data["signing_channel"]["name"]
        except:
            bywho = json_data["name"]

        try:
            ftype = json_data["value"]["stream_type"]
        except:
            ftype = json_data["value_type"]

    return [
        confirm,
        amount,
        tip,
        bywho,
        publication,
        ftype,
        publication_url,
        image_url,
        claim_id,
    ]


def single_file(i, server="http://localhost:5279"):
    title = ""
    ftype = "claim"
    bywho = ""
    publication_url = ""
    image_url = ""
    claim_id = ""

    try:
        try:
            title = i["metadata"]["title"]
        except:
            title = i["claim_name"]
        try:
            bywho = i["channel_name"]
        except:
            pass

        try:
            ftype = i["metadata"]["stream_type"]
        except:
            pass

        publication_url = i["claim_name"] + "#" + i["claim_id"]

        try:
            image_url = i["metadata"]["thumbnail"]["url"]
        except:
            pass

        try:
            claim_id = i["claim_id"]
        except:
            pass

    except:
        pass
    return [bywho, title, ftype, publication_url, image_url, claim_id]


def single_wallet(i, server="http://localhost:5279"):
    id = ""
    name = ""
    try:
        id = i["id"]
    except:
        pass

    try:
        name = i["name"]
    except:
        pass

    return [id, name]


parsers = {
    "publication": single_publication,
    "comment": single_comment,
    "channel": single_channel,
    "balance": single_balance,
    "wallet": single_wallet,
    "file": single_file,
}


def publications(json_data, server="http://localhost:5279"):
    return parse(json_data, "publication", server=server)


def comments(json_data, server="http://localhost:5279"):
    return parse(json_data, "comment", server=server)


def channels(json_data, server="http://localhost:5279"):
    return parse(json_data, "channel", server=server)


def wallets(json_data, server="http://localhost:5279"):
    return parse(json_data, "wallet", server=server)


def balances(json_data, server="http://localhost:5279"):
    return parse(json_data, "balance", server=server)


def files(json_data, server="http://localhost:5279"):
    return parse(json_data, "file", server=server)


def parse(json_data, parse_type, server="http://localhost:5279"):
    # Now we want to parse the json

    try:
        data = []

        # List what we found
        for n, i in enumerate(json_data["items"]):
            single_data = parsers[parse_type](i, server=server)
            if isinstance(single_data, str):
                return single_data

            data.append(single_data)

        return data

    # Error messages
    except Exception as e:
        if "error" in json_data:
            return error.error(e, json_data["error"])
        return error.error(e, json_data)
