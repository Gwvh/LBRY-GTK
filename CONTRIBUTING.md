# Rules of Contributing:

- Use PascalCase in Source, snake_case in flbry.
- When working on the GUI, use the Glade files as much as possible.
- When all work is done, squash your commits, before merge.
